import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime
from sklearn import linear_model
from sklearn.metrics import r2_score, explained_variance_score
from sklearn.metrics import mean_squared_error, mean_absolute_error

def calc_perf_metrics(test_labels, preds, num_features=None, precision=3,
                      omit=[]
    ):
    # init accumulator for metrics
    metrics = {}

    print('-'*50)

    # calculate residuals
    try:
        test_labels = test_labels.flatten()
    except AttributeError as e:
        test_labels = test_labels.values.flatten()

    try:
        preds = preds.flatten()
    except AttributeError as e:
        preds = preds.values.flatten()

    test_resids = test_labels - preds

    # calculate performance metrics
    RSS = sum(test_resids**2)

    # mean squared error
    MSE = mean_squared_error(test_labels, preds)

    print(f'Mean Squared Error: {MSE:.2e}')
    metrics['MSE'] = MSE

    # root mean squared error
    RMSE = np.sqrt(MSE)
    print(f'Root Mean Squared Error: {RMSE:.2e}')
    metrics['RMSE'] = RMSE

    # mean absolute error
    MAE = np.mean(abs(test_resids))
    print(f'Mean Absolute Error: {MAE:.2e}')
    metrics['MAE'] = MAE

    r_squared = r2_score(test_labels, preds)
    print(f'R-squared: {r_squared:.2e}')
    metrics['r_squared'] = r_squared

    # mean value of target in test set
    target_mean = np.mean(abs(test_labels))
    print(f'Mean of target values: {target_mean:.2e}')
    metrics['target_mean'] = target_mean

    # median value of target in test set
    target_median = np.median(abs(test_labels))
    print(f'Mean of target values: {target_median:.2e}')
    metrics['target_median'] = target_median


    if 0 in test_labels:
        accuracy = 'Undefined'
    else:
        APE = 100 * (abs(test_resids/test_labels))
        MAPE = np.mean(APE)
        accuracy = 100 - MAPE
        print('Accuracy: {} %'.format(accuracy.round(precision)))

    metrics['accuracy'] = accuracy

    # compute explained variance
    metrics['explained_variance'] = explained_variance_score(test_labels, preds)
    # compute R^2 and  adjusted R^2
    SS_Residual = sum(test_resids**2)
    SS_Total = sum((test_labels-np.mean(test_labels))**2)
    if 'r_squared' not in omit:
        metrics['r_squared'] = r2_score(test_labels, preds)
        if num_features != None:
            adj_r_squared = 1 - ((1-r_squared)*(len(test_labels)-1) /
                                     (len(test_labels)-num_features-1))
            print(f'R^2: {r_squared:.2e}')
            print(f'Adj. R^2: {adj_r_squared:2e}')
            metrics['adj_r_squared'] = adj_r_squared
        else:
            adj_r_squared = None

    print('-'*50)

    return(metrics)

def parity_plot_grid(datasets, tag='plot', export_fig=True,
                     fig_qual=90, fig_dpi=200,
    ):
    fig, axs = plt.subplots(1, len(datasets), figsize=(18,5))

    for i in range(len(datasets)):
        d = datasets[i]
        parity_plot(axs[i], datasets[i]['x'], datasets[i]['y'],
                         x_title=d['x_title'],
                         y_title=d['y_title'],
        )
    if export_fig:
        fig.savefig(f'../plots/parity_plot_grid-{tag}.png',
                    quality=fig_qual, dpi=fig_dpi)

def parity_plot(ax, labels, preds, x_title, y_title, tag='plot',
                textstr='', plot_title='',
                export_fig=False, fig_qual=90, fig_dpi=200,
    ):
    if export_fig:
        fig, ax = plt.subplots()

    pt_size = 2.5
    pt_trans = 0.9 # tranparency of points (0, transparent to 1, opaque)
    lw = 1
    lr = linear_model.LinearRegression()
    preds = np.array(preds).flatten()
    labels = np.array(labels).flatten()

    regplot_scatter_kws = {
        's':pt_size,
        'alpha': pt_trans,
        'color': 'black'
        # 'color': 'green'
    }
    regplot_line_kws = {
            'lw':lw,
            'color': 'dodgerblue'
        }
    sns.regplot(labels, preds, scatter_kws=regplot_scatter_kws,
                line_kws=regplot_line_kws, ax=ax
    )
    ax.plot([labels.min(), labels.max()],
            [labels.min(), labels.max()],
            'k--',
            lw=lw,
            color='red')

    ax.set_xlabel(x_title)
    ax.set_ylabel(y_title)

    if plot_title:
        ax.set_title(plot_title)

    if textstr:
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12,
            verticalalignment='top',
            horizontalalignment='left',
            bbox=props)

    if export_fig:
        fig.savefig(f'../plots/parity_plot_-{tag}.png',
                    quality=fig_qual, dpi=fig_dpi)
    return(ax)

def multi_histogram(val_dicts, tag='plot', fig_qual=90, fig_dpi=200):
    pal = sns.husl_palette(10)
    colors = []
    for i in [0,3,7]:
        colors.append(pal[i])
    fig, ax = plt.subplots()
    i = 0
    for k in val_dicts.keys():
        val_list = list(val_dicts[k])
        sns.distplot(val_list, color=colors[i], label=k)
        plt.legend()
        i += 1

    # config axes
    ax.set_xlabel('Predicted MSP ($/gal)')
    ax.set_ylabel('Frequency')

    # ax.set_xlabel('SuperPro Predicted MSP')
    # ax.set_ylabel('Frequency')
    fig.savefig(f'../plots/multi_histogram-{tag}.png',
                quality=fig_qual, dpi=fig_dpi)

if __name__ == '__main__':
    main()


def main():
    # test calc_perf_metrics function
    test_labels = np.random.rand(1, 10).flatten()
    preds = np.random.rand(1, 10).flatten()
    calc_perf_metrics(test_labels, preds)
    exit()
if __name__ == '__main__':
    main()
