from pymongo import MongoClient
import json
from bson.binary import Binary
import gzip
import joblib
import os

class ClientMaker(MongoClient):
    """
    Subclasses MongoClient to instantiate a client object with a default
    connection string.
    """

    def __init__(self,
                 conn_str='mongodb+srv://testRWuser:jtsnow06@cluster0-rpvbw.' \
                          'mongodb.net/test?retryWrites=true'):

        super().__init__(conn_str)
        # self.conn_str = conn_str
        # return self.yield_client()

    # def yield_client(self):
    #     return MongoClient(self.conn_str)/


class MongoLoader:
    """
    Base class for batch loading objects to MongoDB collections.
    """

    def __init__(self, conn_str, db):
        self.conn_str = conn_str
        self.client = MongoClient(self.conn_str)
        self.db = self.client[db]
        self.queued_docs = []

    def modify_queued_documents(self, unary_fn):
        """
        Apply a function to modify a queued document before it is inserted into
        a collection.

        Args:
        ---
            unary_fn (function): a function of one argument to be applied to
                the queued document.
        """
        self.queued_docs = [unary_fn(d) for d in self.queued_docs]

    def insert_queued_docs_to_collection(self, coll, uid_field):
        # load the json file
        coll = self.db[coll]
        for doc in self.queued_docs:
            try:
                uid = doc[uid_field]
            except:
                continue
            # only insert if doc with this PLID does not already exist in coll
            if coll.count({uid_field: uid}) == 0:
                coll.insert_one(doc)
                self.queued_docs.remove(doc)
                print(f'Loaded doc with {uid_field}: {uid} into coll: {coll}!')
            else:
                print(f'Doc with {uid_field}: {uid} already in coll: {coll}!')


class PipelineToMongoLoader(MongoLoader):
    """
    Loads joblib serialized pipeline and associated metadata to
    specified collections of a MongoDB instance.

    """

    def __init__(self, conn_str, db, meta_coll, serial_coll):

        # super().__init__(conn_str, db)
        self.meta_coll = meta_coll
        self.serial_coll = serial_coll
        self.joblib_loader = JoblibToMongoLoader(conn_str, db)
        self.json_loader = JsonToMongoLoader(conn_str, db)

    def load_serial_pipelines_and_metadata_to_mongodb(self, pl_mdata_fps):

        for fp in pl_mdata_fps:
            serial_pls = os.listdir('../neural_net_exports')
            try:
                plid = extract_plid(fp)
            except:
                continue
            # check that there is exactly one matching serialized pipeline
            if len([f for f in serial_pls if plid in f]) == 1:
                # print(f'Loading metadata for pipeline with PLID: {plid}...')

                self.json_loader.load_json_to_queued_document(fp)
                self.json_loader.insert_queued_docs_to_collection(
                    self.meta_coll, uid_field='plid'
                )
                serial_fp = f'../neural_net_exports/' \
                            f'{[f for f in serial_pls if plid in f][0]}'
                print(f'Loading serialization of PLID: {plid}...')
                self.joblib_loader.load_joblib_to_queued_document(serial_fp)
                self.joblib_loader.insert_queued_docs_to_collection(
                    self.serial_coll, uid_field='plid_c'
                )


class JsonToMongoLoader(MongoLoader):
    """
    Subclass of MongoLoader for batch loading JSON objects to
    MongoDB collections.
    """

    def load_json_to_queued_document(self, json_fp):
        print(f'Queueing JSON file: {json_fp}')
        with open(json_fp, 'r') as f:
            doc = json.load(f)
        self.queued_docs.append(doc)

class JoblibToMongoLoader(MongoLoader):
    """
    Subclass of MongoLoader for batch loading serialized joblib objects to
    MongoDB collections.
    """

    def load_joblib_to_queued_document(self, joblib_fp, chunk_size=15000000):
        # with gzip.GzipFile(joblib_fp, 'r') as f:
        #     obj = f.read()
        with open(joblib_fp, 'rb') as f:
            chunk = f.read(chunk_size)
            chunk_count = 0
            while chunk:
                bytes = open(joblib_fp, 'rb').read()
                doc = {
                    'plid':
                        f"PLID_{extract_plid(joblib_fp)}",
                    'plid_c':
                        f"PLID_{extract_plid(joblib_fp)}_{chunk_count}",
                    'chunk_num': chunk_count,
                    'bytes': Binary(chunk)
                }
                self.queued_docs.append(doc)
                chunk = f.read(chunk_size)
                chunk_count += 1

    @staticmethod
    def joblib_to_binary(joblib_fp):
        bytes = open(joblib_fp, 'rb').read()
        return Binary(bytes)

    def load_serialized_pipepline(self, collection):
        # load the json file
        coll = self.db[collection]
        filter = {'tag':'binary'}
        bytes = coll.find(filter)[0]['binary_data']
        tfp = 'tempfile.joblib'
        with open(tfp, 'wb') as f:
            f.write(bytes)
        pline = joblib.load(tfp)
        os.remove(tfp)
        return pline


def test_JoblibToMongoLoader():
    s = 'mongodb+srv://testRWuser:jtsnow06@cluster0-rpvbw.mongodb.net/' \
        'test?retryWrites=true'
    db = 'f2ft-property-prediction-db'
    loader = JoblibToMongoLoader(conn_str=s, db=db)
    c = 'mspLearn_trained_models'
    fps = [
        '../serialized_tpot_pipelines/tpot_pipeline_PLID_4647974.joblib',
        '../serialized_tpot_pipelines/tpot_pipeline_PLID_7795419.joblib'
    ]
    for fp in fps:
        loader.load_joblib_to_queued_document(joblib_fp=fp)

    loader.insert_queued_docs_to_collection(collection=c, uid_field='plid')


def test_JsonToMongoLoader():
    s = 'mongodb+srv://testRWuser:jtsnow06@cluster0-rpvbw.mongodb.net/' \
        'test?retryWrites=true'
    db = 'f2ft-lca-tea-db'
    loader = JsonToMongoLoader(conn_str=s, db=db)
    c = 'mspLearn_trained_models'
    json_fps = [
        '../tpot_search_results/tpot_pipeline_PLID_2131329_metadata.json',
        '../tpot_search_results/tpot_pipeline_PLID_4647974_metadata.json'
    ]
    for json_fp in json_fps:
        loader.load_json_to_queued_document(json_fp=json_fp)

    def fn(x):
        x['pathway_product'] = 'limonane'
        return(x)

    loader.modify_queued_documents(fn)
    loader.insert_queued_docs_to_collection(collection=c)

def test_PipelineToMongoLoader():
    s = 'mongodb+srv://testRWuser:jtsnow06@cluster0-rpvbw.mongodb.net/' \
        'test?retryWrites=true'
    db = 'f2ft-property-prediction-db'
    loader = PipelineToMongoLoader(conn_str=s, db=db,
                                   meta_coll='models',
                                   serial_coll='model_weights'
    )
    dir = '../neural_net_exports'
    json_fps = [f'{dir}/{f}' for f in os.listdir(dir) if f.endswith('.json')]
    # print(json_fps)
    loader.load_serial_pipelines_and_metadata_to_mongodb(json_fps)


def initial_db_load(coll):
    s = 'mongodb+srv://testRWuser:jtsnow06@cluster0-rpvbw.mongodb.net/' \
        'test?retryWrites=true'
    db = 'f2ft-lca-tea-db'
    loader = JsonToMongoLoader(conn_str=s, db=db)
    # json_fps = [

    #     '../tpot_search_results/tpot_pipeline_PLID_2131329_metadata.json',

    #     '../tpot_search_results/tpot_pipeline_PLID_4647974_metadata.json'

    # ]
    json_fps = [
        f'../tpot_search_results/{f}'
        for f in os.listdir('../tpot_search_results')
        if 'PLID' in f and '.json' in f
    ]

    for json_fp in json_fps:
        loader.load_json_to_queued_document(json_fp=json_fp)

    def fn(x):
        x['end_product'] = 'limonane'
        x['pathway'] = 'limonene'
        fp = x.pop('serialized_pipeline_fp')
        binary_data = JoblibToMongoLoader.joblib_to_binary(fp)
        x['binary_joblib_serialization'] = binary_data
        x['validation_size'] = 0.2
        x['n_samples_dataset'] = 10000
        x.pop('filename')

        return(x)

    loader.modify_queued_documents(fn)
    loader.insert_queued_docs_to_collection(collection=coll)

def extract_plid(s):
    return(''.join([c for c in [i for i in s] if c.isdigit()]))



def main():
    # initial_db_load(coll='mef_trained_pipelines')
    # test_JoblibToMongoLoader()
    test_PipelineToMongoLoader()


if __name__ == '__main__':
    main()
