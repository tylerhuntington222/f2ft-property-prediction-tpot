from tpot import TPOTRegressor
import seaborn as sns
sns.set()
import joblib
from build_simple_dnn import *
import absl.logging
import logging
import json
import argparse
import pandas as pd
import numpy as np
from ml_utils import calc_perf_metrics
from datetime import datetime
from collections import Counter
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.base import BaseEstimator
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.ensemble import IsolationForest, VotingRegressor
from sklearn.impute import SimpleImputer
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn.metrics import mean_squared_error

from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVR

from yellowbrick.model_selection import CVScores
from visualizers import KFoldCVScoresViz
import pandas as pd
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)

# DATA_FNAME = 'fdf_9_19_19_descriptors_and_fragments_no_CHON.csv'
# DATA_FNAME = 'fdf_9_19_19_shortened_descr.csv'
# DATA_FNAME = 'fdf_9_19_19_shortened_descr_no_CHON.csv'

RUN_TAG = ''
N_IMP_FEATS_RF = 20
# will override N_IMP_FEATS_RF if given
SELECT_FEATURES_BY_RF_IMPORTANCE = True
RF_IMP_FEAT_CUM_THRESH = 1

EXPORT_OUTLIERS = False
EXPORT_WEIGHTS = True

RANDOM_SEED = 42
N_CV_FOLDS = 5
IQR_OUTLIER_REMOVAL = False
ISO_FOR_OUTLIER_REMOVAL = False
ISO_FOR_NTREE = 100
RF_NTREE = 500
N_JOBS = -1
TARGETS = [
    # 'cn_coalesced',
    # 'fp_c_coalesced',
    # 'ysi_coalesced',
    'bp_c_coalesced',
    'mp_c_coalesced',
    'dcn_coalesced',
]

# TPOT configs
MAX_TIME_MINS = 12
MAX_EVAL_TIME_MINS = 1

logging.root.removeHandler(absl.logging._absl_handler)
absl.logging._warn_preinit_stderr = False
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s',
                              datefmt='%m-%d %H:%M'
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)



def main():


    # parse command line args
    parser = argparse.ArgumentParser(
        description='Script to build and evaluate a simple Deep Neural Net.'
    )

    parser.add_argument('--test_size', '-ts', action='store', default=0.2)
    parser.add_argument('--validation_size', '-v', action='store', default=0.2)
    parser.add_argument('--n_epochs', '-e', action='store', default=300)
    parser.add_argument('--batch_size', '-b', action='store', default=50)
    parser.add_argument('--target', '-tg',
                        help="Target variable to evaluate.",
                        default=''
                        )
    parser.add_argument('--input_units', '-u_in', action='store',
                        default='dol_per_lit')
    parser.add_argument('--output_units', '-u_out', action='store',
                        default='degrees C'
                        )
    parser.add_argument('--dim_reduce_factor', '-d', action='store',
                        default='-1'
                        )
    parser.add_argument('--outlier_contamination', '-oc', action='store',
                        default='-1'
                        )
    parser.add_argument('--build_fn_name', '-bfn', action='store',
                        default=''
                        )
    parser.add_argument('--verbosity', '-vb', action='store',
                        default='0'
                        )
    parser.add_argument('--rm_outliers_by_iqr', '-iqr', action='store',
                        default='1'
                        )
    parser.add_argument(
        '--impute', '-imp', action='store',
        help="Empty string (for no imputation) or `simple` for mean imputation",
        default=''
    )

    prop_dim_reduce_factors = {
        'bp_c_coalesced': 0.0001,
        'fp_c_coalesced': 0.0001,
        'mp_c_coalesced': 0.0001,
        'cn_coalesced': 0.0001,
        'dcn_coalesced': 0.0001,
        'ysi_coalesced': 0.05,
    }

    outlier_contam_factors = {
        'bp_c_coalesced': 0.001,
        'fp_c_coalesced': 0.001,
        'mp_c_coalesced': 0.001,
        'cn_coalesced': 0.001,
        'dcn_coalesced': 0.001,
        'ysi_coalesced': 0.001,
    }

    iqr_outlier_factors = {
        'bp_c_coalesced': 2,
        'fp_c_coalesced': 3,
        'mp_c_coalesced': 2,
        'cn_coalesced': 2,
        'dcn_coalesced': 2,
        'ysi_coalesced': 3,
    }

    # TODO: perform lookups in this dict when building NN for each target
    impute_approaches = {
        'bp_c_coalesced': '',
        'fp_c_coalesced': '',
        'mp_c_coalesced': '',
        'cn_coalesced': '',
        'dcn_coalesced': '',
        'ysi_coalesced': '',
    }

    args = parser.parse_args()
    n_epochs = int(args.n_epochs)
    # logger.info(f'DNN training will last {n_epochs} epochs')
    batch_size = int(args.batch_size)
    logger.info(f'Batch size: {batch_size}')
    # default to list of all properties as targets if none specified as CL arg
    if not args.target:
        targets = TARGETS
    # put user-specified target into list to match default data structure
    else:
        targets = [args.target]

    # TODO: make this a dict lookup based on the target
    output_units = args.output_units
    test_size = float(args.test_size)
    validation_size = float(args.validation_size)
    outlier_contam = float(args.outlier_contamination)
    impute = args.impute
    verbosity = int(args.verbosity)
    rm_outliers_by_iqr = bool(int(args.rm_outliers_by_iqr))

    dim_reduce_factor = float(args.dim_reduce_factor)

    # parse name of build function to determine model architecture
    build_fn_name = args.build_fn_name


    # TODO: remove temp overrride
    for target in targets:
        # # load data
        # data_fp = f'../data/{DATA_FNAME}'

        # TODO: remove temp overrride
        DATA_FNAME = f'no_outlier_mad_{target}_s.csv'
        data_fp = f'../data/{DATA_FNAME}'
        df = pd.read_csv(data_fp)

        ### basic data cleaning:
        # drop extranneous index columns
        keep_cols = [c for c in df.columns if 'Unnamed: ' not in c]
        df = df[keep_cols]

        # # rename cols of df according to metadata JSON
        # fp = f'{data_fp[:-4]}_metadata.json'
        # with open(fp, 'r') as f:
        #     mdata = json.load(f)
        # rename_dict = mdata['colname_map']
        # df = df.rename(rename_dict, axis=1)

        # # get target cols defined by metadata file
        # target_cols = mdata['target_cols']

        #
        ### define feature cols:
        # assumes that all candidate features are the right-most columns in df
        # and the first feature column name is prefixed with `mordred_desc_`

        # find index of first feature col
        mordred_descs = [c for c in df.columns if 'mordred_desc' in c]
        first_feat_index = list(df.columns).index(mordred_descs[0])
        all_feats = list(df.columns[first_feat_index:df.shape[1]])

        # assign master_df pointer to dataset for easier copying in sensitivity runs
        master_df = df

        target_aliases = {
            'bp_c_coalesced': 'Boiling Point (deg C)',
            'fp_c_coalesced': 'Flash Point (deg C)',
            'mp_c_coalesced': 'Melting Point (deg C)',
            'cn_coalesced': 'Cetane Number',
            'dcn_coalesced': 'DCN',
            'ysi_coalesced': 'YSI',
        }


        ### basic data cleaning:
        # drop extranneous index columns
        keep_cols = [c for c in df.columns if 'Unnamed: ' not in c]
        df = df[keep_cols]


        # # rename cols of df according to metadata JSON
        # fp = f'{data_fp[:-4]}_metadata.json'
        # with open(fp, 'r') as f:
        #     mdata = json.load(f)
        # rename_dict = mdata['colname_map']
        # df = df.rename(rename_dict, axis=1)

        # # get target cols defined by metadata file
        # target_cols = mdata['target_cols']

        #
        ### define feature cols:
        # assumes that all candidate features are the right-most columns in df
        # and the first feature column name is prefixed with `mordred_desc_`

        # find index of first feature col
        mordred_descs = [c for c in df.columns if 'mordred_desc' in c]
        first_feat_index = list(df.columns).index(mordred_descs[0])
        all_feats = list(df.columns[first_feat_index:df.shape[1]])

        # assign master_df pointer to dataset for easier copying in sensitivity runs
        master_df = df

        target_aliases = {
            'bp_c_coalesced': 'Boiling Point (deg C)',
            'fp_c_coalesced': 'Flash Point (deg C)',
            'mp_c_coalesced': 'Melting Point (deg C)',
            'cn_coalesced': 'Cetane Number',
            'dcn_coalesced': 'DCN',
            'ysi_coalesced': 'YSI',
        }
        # TODO: comment this start to the for-loop back in
        # for target in targets:
        # TODO: remove temp for loop for sensitivity run over thresh vals
        # for cum_imp_thresh in [0.6, 0.65, 0.7, 0.75,  0.8, 0.85, 0.9, 0.95]:
        for cum_imp_thresh in [RF_IMP_FEAT_CUM_THRESH]:
            logger.info(f'Using cumulative imp. thresh: {cum_imp_thresh}')

            target_alias = target_aliases[target]
            logger.info(f'Building model for target: {target}')
            df = master_df.copy(deep=True)

            # determine dimensionality reduction factor to use
            if dim_reduce_factor == -1:
                dim_reduce_factor = prop_dim_reduce_factors[target]

            # determine outlier contamination factor to use
            if outlier_contam == -1:
                outlier_contam = outlier_contam_factors[target]

            # drop rows with NA target values
            logger.info(f'Original shape of df: {df.shape}')
            df = df.dropna(subset=[target])
            logger.info(
                f'Shape after dropping samples with NA target: {df.shape}'
            )

            # compute counts of unique values in feature cols
            feat_val_summaries = compute_column_summaries(df, all_feats)

            logger.info(f'Using dim_reduce_factor: {dim_reduce_factor}')
            logger.info(f'Using outlier_contam: {outlier_contam}')

            feats_to_keep = drop_trivial_feature_cols(
                df, feat_val_summaries, dim_reduce_factor
            )

            # drop additional columns using RF variable importances
            if SELECT_FEATURES_BY_RF_IMPORTANCE:
                feats_to_keep = drop_features_by_rf_feat_imp(
                    df, feats_to_keep, target=target,
                    cumulative_thresh=cum_imp_thresh,
                    n_estimators=RF_NTREE, n_jobs=N_JOBS
                )

            feats_to_drop = [c for c in all_feats if c not in feats_to_keep]
            feat_cols = feats_to_keep

            # makelist of feature columns with non-stationary features
            n = len(feat_cols); m = len(all_feats)
            logger.info(f'Using {n}/{m} of total features.')

            # drop feature columns flagged for removal
            df = df[[c for c in df.columns if c not in feats_to_drop]]
            logger.info(
                f'Shape after feature removal: {df.shape}'
            )

            # drop samples with NA for any feature
            if not impute:
                logger.info('No imputation being performed.')
                logger.info('Dropping samples with any NA feature vals')
                df = df.dropna(subset=feat_cols)
                logger.info(
                    f'Samples with NA feature vals removed'
                    f'Shape of df: {df.shape}'
                )
            elif impute == 'simple':
                logger.info(
                    'Using simple mean imputation for missing feature vals'
                )
                imputer = SimpleImputer()
                df[feat_cols] = imputer.fit_transform(df[feat_cols])

            # if DROP_CHON_CMPDS:
            #     df str.contains('B|Br|Cl|F|S|I|P|Si|s|Cr|cr|Hg|Mg|Ti')]

            iqr_outliers_removed = 0
            if IQR_OUTLIER_REMOVAL:
                # identify and remove outliers based on target vals
                logger.info(
                    f'Removing outliers based on target values using IQR method'
                )
                pre_shape = df.shape
                iqr_fac = iqr_outlier_factors[target]
                df = remove_outliers_by_iqr(df, target, iqr_fac)
                logger.info(
                    f'Shape after removing outliers by IQR method: {df.shape}'
                )
                iqr_outliers_removed = pre_shape[0] - df.shape[0]

            iso_for_outliers_removed = 0
            if ISO_FOR_OUTLIER_REMOVAL:
                # identify and remove outliers based on feature vals
                logger.info(
                    f'Removing outliers based on feature values using Iso. Forest'
                )
                pre_shape = df.shape
                df = remove_outliers_by_isolation_forest(
                    df, feat_cols, outlier_contam, target
                )
                logger.info(
                    f'Shape after removing outliers by Iso. Forest: {df.shape}'
                )
                iso_for_outliers_removed = pre_shape[0] - df.shape[0]

            # extract features and labels from modeling df
            features = df.loc[:, feat_cols]
            labels = df.loc[:, target]

            print()
            logger.info(f'(Target: {target})')
            logger.info(f'(Mean of target vals: {np.mean(labels)})')
            logger.info(f'(Std.Dev. of target vals: {np.std(labels)})')
            print()
            fig, ax = plt.subplots()
            sns.distplot(labels, ax=ax)
            ax.set_xlabel(target_alias)
            ax.set_ylabel('Frequency')

            text_str = (
                f'Median = {np.median(labels):0.3f}'
                f'\nMean = {np.mean(labels):0.3f}'
                f'\nStd. Dev. = {np.std(labels):0.3f}'
            )

            # these are matplotlib.patch.Patch properties
            props = dict(boxstyle='round', facecolor='gray', alpha=0.5)
            ax.text(0.7, 0.95, text_str, transform=ax.transAxes,
                    fontsize=14,
                    verticalalignment='top', bbox=props)

            fig.savefig(f'../plots/distplot_{target}.png')

            # TODO: temp, remove
            # continue

            # simple train test split
            # X_train, X_test, y_train, y_test = train_test_split(features, labels,
            #                                                     test_size=test_size,
            #                                                     random_state=RANDOM_SEED
            #                                                     )

            outer_kfold_cv = KFold(n_splits=N_CV_FOLDS,
                                   shuffle=True,
                                   random_state=RANDOM_SEED
            )

            inner_kfold_cv = KFold(n_splits=N_CV_FOLDS,
                                   shuffle=True,
                                   random_state=RANDOM_SEED
            )
            tpot = TPOTRegressor(
                verbosity=1,
                max_time_mins=MAX_TIME_MINS,
                max_eval_time_mins=MAX_EVAL_TIME_MINS,
                random_state=RANDOM_SEED,
                use_dask=True,
                n_jobs=N_JOBS,
                cv=inner_kfold_cv,
                memory='auto',
            )

            # cast all features to floats
            for f in features.columns:
                features[f] = features[f].astype(float)

            # determine unique identifier for this pipeline (set)
            plid = str(datetime.now())[2:19]
            plid = plid.replace(':', '').replace(' ', '').replace('-', '')

            # serialize and export optimal pipeline
            dir = '../tpot_search_results/'
            serial_pline_fp_pfx = f'{dir}tpot_pline_ens_learner_{plid}'

            # init accumulator for learners (pipelines optimized for each fold)
            learners = []

            # init fold counter
            fold = 0
            mse_vals = []
            # iterate through outer CV folds
            for train_idx, test_idx in outer_kfold_cv.split(features):
                X_train = features.iloc[train_idx]
                X_test = features.iloc[test_idx]
                y_train = labels.iloc[train_idx]
                y_test = labels.iloc[test_idx]

                # run TPOT optimization
                tpot.fit(X_train, y_train)

                # extract fitted pipeline from TPOT regressor after optimization
                pipeline = tpot.fitted_pipeline_
                logger.info(f'Best Pipeline: {pipeline}')
                learners += [(f'learner_{fold}', pipeline)]

                # serialize and export pipeline learner
                serial_pipeline_fp = f'{serial_pline_fp_pfx}-{fold}.joblib'
                joblib.dump(tpot.fitted_pipeline_, serial_pipeline_fp)

                mse = tpot.score(X_test, y_test)
                logger.info(f'Test MSE: {mse}')
                mse_vals += [mse]

                fold += 1

            # compute RMSE vals for folds
            rmse_vals = [np.sqrt(abs(x)) for x in mse_vals]

            # rename features to play nicely with VotingRegressor...
            features.columns = [f'feat_{i}' for i in range(features.shape[1])]

            # make ensemble model from learners optimized to each CV fold
            ensemble = VotingRegressor(learners)
            ens_pipeline_fp = f'{serial_pline_fp_pfx}_ens.joblib'
            joblib.dump(ensemble, ens_pipeline_fp)

            # evaluate ensemble (note: metrics biased by leakage)
            ens_mse_vals = cross_val_score(
                ensemble, features, labels,
                cv=outer_kfold_cv,
                scoring='neg_mean_squared_error'
            )

            # compute RMSE vals for folds
            ens_rmse_vals = [np.sqrt(abs(x)) for x in ens_mse_vals]
            logger.info(f'Ensemble CV RMSE Values (biased): {ens_rmse_vals}')

            # create barplot viz of fold CV scores
            fig, ax = plt.subplots()
            visualizer = KFoldCVScoresViz(
                ax=ax,
                cv=outer_kfold_cv,
                cv_scores=rmse_vals,
                score_metric='RMSE',
                target=target_alias,
                modtype='ANN',
            )
            visualizer.draw()
            visualizer.poof(outpath=f'../plots/kfold_cv_scores_plid_{plid}.png')

            # compute mean and standard deviation of unbiased learners CV RMSE
            mean_rmse = np.mean(rmse_vals)
            std_dev_rmse = np.std(rmse_vals)

            # compute mean and standard deviation of (biased) ensemble CV RMSE
            ens_mean_rmse = np.mean(ens_rmse_vals)
            ens_std_dev_rmse = np.std(ens_rmse_vals)

            kfold_metrics = {
                'n_folds': N_CV_FOLDS,
                'fold_mse_vals': list(mse_vals),
                'fold_rmse_vals': list(rmse_vals),
                'mean_rmse': mean_rmse,
                'std_dev_rmse': std_dev_rmse,
                'ens_fold_rmse_vals': list(ens_rmse_vals),
                'ens_mean_rmse': ens_mean_rmse,
                'ens_std_dev_rmse': ens_std_dev_rmse,
            }

            logger.info('-' * 50)
            logger.info(f'Performance Metrics for: {target}')
            logger.info('-' * 50)
            logger.info(f'Mean Test RMSE: {mean_rmse}')
            logger.info(f'Std. Dev. Test RMSE: {std_dev_rmse}')
            logger.info(f'Mean Ensemble RMSE (biased): {ens_mean_rmse}')
            logger.info(f'Std. Dev. Ensemble RMSE (biased): {ens_std_dev_rmse}')
            logger.info(f'(Mean of target vals: {np.mean(labels)})')
            logger.info(f'(Std.Dev. of target vals: {np.std(labels)})')
            logger.info('-' * 50 + '\n')
            logger.info(f'Test RMSE Values: {rmse_vals}')

            # generate timestamp
            ts = str(datetime.now())[:-10].replace(' ', '_')

            # generate model run results object
            mod_obj = {
                'input_data_fname': DATA_FNAME,
                'dim_reduce_factor': dim_reduce_factor,
                'outlier_removal': {
                    'target': {
                        'iqr_method': {
                            'applied': str(rm_outliers_by_iqr),
                            'n_outliers_removed': iqr_outliers_removed
                        }
                    },
                    'features': {
                        'isolation_forest': {
                            'contamination': outlier_contam,
                            'n_estimators': ISO_FOR_NTREE,
                            'n_outliers_removed': iso_for_outliers_removed
                        }
                    }
                },
                'feature_selection': {
                    'rf_feat_importances': {
                        'cumulative_imp_thresh': RF_IMP_FEAT_CUM_THRESH,
                    }
                },
                'imputation': {
                    'approach': impute,
                },
                'plid': plid,
                # 'weights_filename': wts_fname,
                'eval_metrics': {
                        'kfold_metrics': kfold_metrics,
                },
                'validation_size': validation_size,
                'max_time_mins': MAX_TIME_MINS,
                'max_eval_time_mins': MAX_EVAL_TIME_MINS,
                'timestamp': ts,
                'output_units': output_units,
                'target': target,
                'n_samples': df.shape[0],
                'n_features': len(feat_cols)
            }
            # print(mod_obj)

            mod_json_fp = f'../tpot_search_results/tpot_mdata_plid_{plid}.json'
            with open(mod_json_fp, 'w') as f:
                json.dump(mod_obj, f)


if __name__ == '__main__':
    main()
