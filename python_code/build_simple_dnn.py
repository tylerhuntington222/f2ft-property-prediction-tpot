import seaborn as sns
sns.set()
import absl.logging
import logging
import json
import argparse
import pandas as pd
import numpy as np
from ml_utils import calc_perf_metrics
from datetime import datetime
from collections import Counter
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Conv2D, BatchNormalization, Dropout, Flatten
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.base import BaseEstimator
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.ensemble import IsolationForest
from sklearn.impute import SimpleImputer
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVR

from yellowbrick.model_selection import CVScores
from visualizers import KFoldCVScoresViz
import pandas as pd
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)

# TODO: use a global dataset
# DATA_FNAME = 'fdf_9_19_19_descriptors_and_fragments.csv'
# DATA_FNAME = 'fdf_9_19_19_shortened_descr_no_CHON.csv'


N_IMP_FEATS_RF = 20
# will override N_IMP_FEATS_RF if given
SELECT_FEATURES_BY_RF_IMPORTANCE = False
RF_IMP_FEAT_CUM_THRESH = 1

EXPORT_OUTLIERS = False
EXPORT_WEIGHTS = True

IQR_OUTLIER_REMOVAL = False
ISO_FOR_OUTLIER_REMOVAL = False

RANDOM_SEED = 42
N_CV_FOLDS = 5
ISO_FOR_NTREE = 100
RF_NTREE = 500
N_JOBS = -1
TARGETS = [
    'cn_coalesced',
    'ysi_coalesced',
    'fp_c_coalesced',
    'bp_c_coalesced',
    'mp_c_coalesced',
    'dcn_coalesced',
]


logging.root.removeHandler(absl.logging._absl_handler)
absl.logging._warn_preinit_stderr = False
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s',
                              datefmt='%m-%d %H:%M'
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

def make_feature_importance_df(mod, train_features, make_relative=False,
                               **kwargs):
    """
    Extracts feature importances from a tree-based model such as
    RandomForest or ExtraTrees and returns a dataframe of features
    and absolute or relative importances based on Gini impurities.
    """
    # Plot feature importance
    feature_importance = mod.feature_importances_
    # make importances relative to max importance
    if make_relative:
        max_imp = feature_importance.max()
        feature_importance = 100.0 * (feature_importance / max_imp)
    df = pd.DataFrame(
        {'feature': train_features, 'importance': feature_importance}
    )
    df = df.sort_index(by='importance', ascending=False).reset_index(drop=True)
    df['cumulative_importance'] = np.cumsum(df['importance'].values)
    return df

def drop_features_by_rf_feat_imp(df, train_features, target, n_keep=None,
                                 cumulative_thresh=None, **kwargs):
    """
    Returns `n_feats` top most important predictors based on Gini impurities
    compute from Random Forest Regression.
    """
    rf = RandomForestRegressor(**kwargs)
    mdf = df.dropna(subset=train_features).copy(deep=True)
    rf.fit(mdf[train_features], mdf[target])
    imp_df = make_feature_importance_df(rf, train_features)

    if cumulative_thresh:
        imp_df = imp_df[imp_df.cumulative_importance <= cumulative_thresh]
    else:
        imp_df = imp_df.iloc[:n_keep,:]

    return list(imp_df.feature)

def drop_trivial_feature_cols(df, feat_col_summaries, dim_reduce_factor):
    """
    Returns a list of feature columns that should be dropped from df based
    on the dimensionality reduction factor passed by caller and
    other criteria contained within control logic of the funtion (i.e. features
    with a single unqiue value for all samples in df)

    Args:
        dim_reduce_factor (float):
            Threshold fraction of allowable NA values in a feature column
            for the feature to be considered (i.e. a value of 0.01 specifies
            that features for which 1% or more of values are NA should be
            omitted. Smaller values will therefore eliminate more features,
            thereby resulting in a greater number of samples with complete
            feature data, yielding a modeling dataset that is longer and
            narrower in shape. Larger values will retain more features at
            the expense of reducing the number of samples that have complete
            feature data, yielding a modeling dataset that is wider and
            shorter in shape.
    TODO: complete docstring
    """
    zero_var_feats = []
    dim_red_feats = []
    for colname, summary in feat_col_summaries.items():
        if summary['total_unique_vals'] == 1:
            zero_var_feats.append(colname)

        if summary['fraction_nan_vals'] > dim_reduce_factor:
            dim_red_feats.append(colname)

    logger.info(f'Features with zero variance: {len(zero_var_feats)}')
    logger.info(f'Features dropped by dim_reduce_factor: {len(dim_red_feats)}')
    drop_feats = dim_red_feats + zero_var_feats
    feat_cols_to_keep = [f for f in feat_col_summaries if f not in drop_feats]

    # if np.isnan(df[f]).any():
    #     print(f'Dropping feature {f} with NA values')
    #     print(df[f].unique())
    #     feat_cols_to_drop.append(f)

    return feat_cols_to_keep


def compute_column_summaries(df, cols):
    """
    Converts all `cols` in df to numeric and returns dict of
    dicts (one for each feat_col) containing summary info such as the
    total count of nan values in the column and total number of unique
    values in the column.
    TODO: complete docstring
    """
    col_summaries_dict = {}

    for f in cols:
        df[f] = pd.to_numeric(df[f], errors='coerce')
        num_unique_vals = len(df[f].unique())
        total_nan_vals = np.isnan(df[f]).sum()
        col_vals_summary = {
            'total_nan_vals': total_nan_vals,
            'total_unique_vals': num_unique_vals,
            'fraction_nan_vals': total_nan_vals/df.shape[0]
        }
        col_summaries_dict[f] = col_vals_summary

    return col_summaries_dict


def remove_outliers_by_iqr(df, target, iqr_fac, export_outliers=EXPORT_OUTLIERS):
    """
    Removes outliers using IQR method. Samples in df whose value in the `target`
    column are less than Q1 - (iqr_fac * IQR)
    or greater than Q3 + (iqr_fac * IQR) are removed from df.
    TODO: complete docstring
    """
    q1 = df[target].quantile(0.25)
    q3 = df[target].quantile(0.75)
    iqr = q3 - q1
    lo_bound = q1 - (iqr_fac * iqr)
    hi_bound = q3 + (iqr_fac * iqr)
    pre_shape = df.shape

    if export_outliers:
        ol_df = df[(df[target] < lo_bound) | (df[target] > hi_bound)]
        ol_fp = f'../data/intermediates/iqr_outliers_{target}.csv'
        ol_df.to_csv(ol_fp, index=False)

    df = df[(df[target] > lo_bound) & (df[target] < hi_bound)]

    iqr_outliers_removed = pre_shape[0] - df.shape[0]
    logger.info(f'IQR Method removed {iqr_outliers_removed} outliers')
    return df

def remove_outliers_by_isolation_forest(df, feat_colnames, contam, target,
                                        export_outliers=EXPORT_OUTLIERS):
    """
    Removes outliers using Isolation Forest method.
    TODO: complete docstring
    """
    iso_for = IsolationForest(n_estimators=ISO_FOR_NTREE,
                              contamination=contam
    )
    iso_for.fit(df[feat_colnames])
    ol_labs = iso_for.predict(df[feat_colnames])
    if export_outliers:
        ol_df = df[ol_labs == -1]
        ol_fp = f'../data/intermediates/isolation_forest_{target}.csv'
        ol_df.to_csv(ol_fp, index=False)
    # remove samples considered outliers by isolation forest
    df = df[ol_labs == 1]
    counts = dict(Counter(ol_labs))
    if -1 in counts:
        logger.info(f'Isolation Forest with contam. factor of {contam} '
              f'removed {counts[-1]} outliers'
        )
    else:
        logger.info(f'No outliers removed by Isolation Forest')

    return df

def plot_loss_history(model, plid, features, labels, target_alias):
    history = model.fit(features, labels)
    history = history.named_steps['regressor'].model.history.history

    # summarize history for loss
    fig, ax = plt.subplots()
    ax.plot(history['loss'])
    ax.set_title(f'Model Loss History {target_alias}')
    ax.set_ylabel('Loss')
    ax.set_xlabel('Epoch')
    ax.legend(['Training'], loc='upper right')
    fig.savefig(f'../plots/loss_history_plid_{plid}.png')

# def build_baseline_model(input_dim):
#     def baseline_model():
#         # build Neural Net
#         nn_model = Sequential()
#
#         # input layer
#         nn_model.add(Dense(128, kernel_initializer='normal',
#                            input_dim=input_dim,
#                            activation='relu'
#                     )
#         )
#
#         # Hidden Layers
#         nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))
#         nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))
#         nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))
#
#         # Output layer
#         nn_model.add(Dense(1, kernel_initializer='normal', activation='linear'))
#
#         # Compile the network
#         nn_model.compile(loss='mean_squared_error', optimizer='adam',
#                          metrics=['mean_squared_error']
#         )
#         return nn_model
#     return baseline_model

def build_cnn_model_0(input_dim, output_dim=1):
    def cnn_model_0():
        model = Sequential()
        model.add(Conv2D(64, kernel_size=(3, 3), activation='relu',
                         padding='same', input_shape=input_dim))
        model.add(Conv2D(64, kernel_size=(3, 3), activation='relu',
                         padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(BatchNormalization())
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(output_dim), activation='linear')

        model.compile(loss='mean_squared_error', optimizer='adam',
                      metrics=['mean_squared_error']
        )
        return model
    return cnn_model_0


def build_ann_model_0(input_dim, output_dim=1):
    def ann_model_0():
        h1 = input_dim
        model = Sequential()
        model.add(
            Dense(
                h1,
                input_dim=input_dim,
                kernel_initializer='normal',
                activation='relu'
            )
        )
        model.add(
            Dense(
                output_dim,
                kernel_initializer='normal',
                activation='linear'
            )
        )
        # Compile the network
        model.compile(
            loss='mean_squared_error',
            optimizer='adam',
            metrics=['mean_squared_error']
        )
        return model
    return ann_model_0

def build_ann_model_1(input_dim, output_dim=1):
    def ann_model_1():
        h1 = int(input_dim/2)
        model = Sequential()
        model.add(Dense(h1, input_dim=input_dim, kernel_initializer='normal', activation='relu'))
        model.add(Dense(int(h1/2)+1, kernel_initializer='normal', activation='relu'))
        model.add(Dense(int(h1/4)+1, kernel_initializer='normal', activation='relu'))
        model.add(Dense(int(h1/6)+1, kernel_initializer='normal', activation='relu'))
        model.add(Dense(output_dim, kernel_initializer='normal', activation='linear'))

        # Compile the network
        model.compile(loss='mean_squared_error', optimizer='adam',
                         metrics=['mean_squared_error']
                         )
        return model
    return ann_model_1

def build_ann_model_2(input_dim, output_dim=1):
    def ann_model_2():

        # init neural net
        nn = Sequential()

        h1 = 2 * input_dim

        # input layer
        nn.add(
            Dense(
                h1,
                kernel_initializer='normal',
                input_dim=input_dim,
                activation='relu'
            )
        )

        # output layer
        nn.add(
            Dense(
                output_dim,
                kernel_initializer='normal',
                activation='linear'
            )
        )

        # compile the network
        nn.compile(
            loss='mean_squared_error',
            optimizer='adam',
            metrics=['mean_squared_error']
        )
        return nn
    return ann_model_2

def build_ann_model_3(input_dim, output_dim=1):
    def ann_model_3():

        # init neural net
        nn = Sequential()

        h1 = 3 * input_dim

        # input layer
        nn.add(
            Dense(
                h1,
                kernel_initializer='normal',
                input_dim=input_dim,
                activation='relu'
            )
        )

        # output layer
        nn.add(
            Dense(
                output_dim,
                kernel_initializer='normal',
                activation='linear'
            )
        )

        # compile the network
        nn.compile(
            loss='mean_squared_error',
            optimizer='adam',
            metrics=['mean_squared_error']
        )
        return nn
    return ann_model_3

def build_ann_model_4(input_dim, output_dim=1):
    def ann_model_4():
        """
        ANN with 1 hidden layer that has 5*input_dim neurons.
        """

        # init neural net
        nn = Sequential()

        h1 = 5 * input_dim

        # input layer
        nn.add(
            Dense(
                h1,
                kernel_initializer='normal',
                input_dim=input_dim,
                activation='relu'
            )
        )

        # output layer
        nn.add(
            Dense(
                output_dim,
                kernel_initializer='normal',
                activation='linear'
            )
        )

        # compile the network
        nn.compile(
            loss='mean_squared_error',
            optimizer='adam',
            metrics=['mean_squared_error']
        )
        return nn
    return ann_model_4


def define_callbacks():
    callbacks = []
    # define a checkpoint callback
    checkpoint_fname = 'wts_mp_c_coalesced_ep_{epoch:03d}.hdf5'
    checkpoint_fpath = f'../neural_net_exports/{checkpoint_fname}'
    checkpoint = ModelCheckpoint(checkpoint_fpath, monitor='val_loss',
                                 save_best_only=True, mode='auto')
    callbacks.append(checkpoint)

    return callbacks

def main():

    # define build function mappings for available architectures
    ALL_BUILD_FUNCS = {
        'build_ann_model_0': build_ann_model_0,
        'build_ann_model_1': build_ann_model_1,
        'build_ann_model_2': build_ann_model_2,
        'build_ann_model_3': build_ann_model_3,
        'build_ann_model_4': build_ann_model_4,
        # 'build_cnn_model_0': build_cnn_model_0,
    }

    # first element in list (value) is best build func so far each target (key)
    best_build_funcs = {
        'bp_c_coalesced': [build_ann_model_0,],
        'fp_c_coalesced': [build_ann_model_0,],
        'mp_c_coalesced': [build_ann_model_0,],
        'cn_coalesced': [build_ann_model_1,],
        'dcn_coalesced': [build_ann_model_1,],
        'ysi_coalesced': [build_ann_model_4,],
    }

    # first element in list (value) is best build func so far each target (key)
    best_build_funcs = {
        'bp_c_coalesced': [build_ann_model_0,],
        'fp_c_coalesced': [build_ann_model_0,],
        'mp_c_coalesced': [build_ann_model_0,],
        'cn_coalesced': [build_ann_model_0,],
        'dcn_coalesced': [build_ann_model_0,],
        'ysi_coalesced': [build_ann_model_0,],
    }

    # # #TODO: remove this temp override to use all build funcs for all targets
    # best_build_funcs = {
    #     'bp_c_coalesced': list(ALL_BUILD_FUNCS.values()),
    #     'fp_c_coalesced': list(ALL_BUILD_FUNCS.values()),
    #     'mp_c_coalesced': [build_ann_model_4,],
    #     'cn_coalesced': list(ALL_BUILD_FUNCS.values()),
    #     'dcn_coalesced': list(ALL_BUILD_FUNCS.values()),
    #     'ysi_coalesced': list(ALL_BUILD_FUNCS.values()),
    # }

    # parse command line args
    parser = argparse.ArgumentParser(
        description='Script to build and evaluate a simple Deep Neural Net.'
    )

    parser.add_argument('--test_size', '-ts', action='store', default=0.2)
    parser.add_argument('--validation_size', '-v', action='store', default=0.2)
    parser.add_argument('--n_epochs', '-e', action='store', default=300)
    parser.add_argument('--batch_size', '-b', action='store', default=50)
    parser.add_argument('--target', '-tg',
                        help="Target variable to evaluate.",
                        default=''
                        )
    parser.add_argument('--input_units', '-u_in', action='store',
                        default='dol_per_lit')
    parser.add_argument('--output_units', '-u_out', action='store',
                        default='degrees C'
                        )
    parser.add_argument('--dim_reduce_factor', '-d', action='store',
                        default='-1'
                        )
    parser.add_argument('--outlier_contamination', '-oc', action='store',
                        default='-1'
                        )
    parser.add_argument('--build_fn_name', '-bfn', action='store',
                        default=''
                        )
    parser.add_argument('--verbosity', '-vb', action='store',
                        default='0'
                        )
    parser.add_argument('--rm_outliers_by_iqr', '-iqr', action='store',
                        default='1'
                        )
    parser.add_argument(
        '--impute', '-imp', action='store',
        help="Empty string (for no imputation) or `simple` for mean imputation",
        default=''
    )

    prop_dim_reduce_factors = {
        'bp_c_coalesced': 0.0001,
        'fp_c_coalesced': 0.0001,
        'mp_c_coalesced': 0.0001,
        'cn_coalesced': 0.0001,
        'dcn_coalesced': 0.0001,
        'ysi_coalesced': 0.05,
    }

    # setting all dim_reduce_factors to zero eliminates all features
    # for which any sample has an NA value (i.e. preserves maximum sample
    # size at the expense of losing more features)
    prop_dim_reduce_factors = {
        'bp_c_coalesced': 0.000,
        'fp_c_coalesced': 0.000,
        'mp_c_coalesced': 0.000,
        'cn_coalesced': 0.000,
        'dcn_coalesced': 0.000,
        'ysi_coalesced': 0.0,
    }

    outlier_contam_factors = {
        'bp_c_coalesced': 0.001,
        'fp_c_coalesced': 0.001,
        'mp_c_coalesced': 0.001,
        'cn_coalesced': 0.001,
        'dcn_coalesced': 0.001,
        'ysi_coalesced': 0.001,
    }

    iqr_outlier_factors = {
        'bp_c_coalesced': 2,
        'fp_c_coalesced': 3,
        'mp_c_coalesced': 2,
        'cn_coalesced': 2,
        'dcn_coalesced': 2,
        'ysi_coalesced': 2,
    }

    # TODO: perform lookups in this dict when building NN for each target
    impute_approaches = {
        'bp_c_coalesced': '',
        'fp_c_coalesced': '',
        'mp_c_coalesced': '',
        'cn_coalesced': '',
        'dcn_coalesced': '',
        'ysi_coalesced': '',
    }

    args = parser.parse_args()
    n_epochs = int(args.n_epochs)
    logger.info(f'DNN training will last {n_epochs} epochs')
    batch_size = int(args.batch_size)
    logger.info(f'Batch size: {batch_size}')
    # default to list of all properties as targets if none specified as CL arg
    if not args.target:
        targets = TARGETS
    # put user-specified target into list to match default data structure
    else:
        targets = [args.target]

    # TODO: make this a dict lookup based on the target
    output_units = args.output_units
    test_size = float(args.test_size)
    validation_size = float(args.validation_size)
    outlier_contam = float(args.outlier_contamination)
    impute = args.impute
    verbosity = int(args.verbosity)
    rm_outliers_by_iqr = bool(int(args.rm_outliers_by_iqr))

    dim_reduce_factor = float(args.dim_reduce_factor)

    # parse name of build function to determine model architecture
    build_fn_name = args.build_fn_name


    # lookup user specified build function and fail if none exists
    if build_fn_name:
        try:
            build_fn = ALL_BUILD_FUNCS[build_fn_name]
        except KeyError:
            e = f"""
                `{build_fn_name}` is not an available build function!
                Must be one of: 
                {[k for k in ALL_BUILD_FUNCS.keys()]}
                """
            raise Exception(e)


    # TODO: remove temp overrride
    for target in targets:
        # load data
        data_fp = f'../data/{DATA_FNAME}'

        # TODO: remove temp overrride
        data_fp = f'../data/no_outlier_mad_{target}_s.csv'
        df = pd.read_csv(data_fp)

        ### basic data cleaning:
        # drop extranneous index columns
        keep_cols = [c for c in df.columns if 'Unnamed: ' not in c]
        df = df[keep_cols]


        # # rename cols of df according to metadata JSON
        # fp = f'{data_fp[:-4]}_metadata.json'
        # with open(fp, 'r') as f:
        #     mdata = json.load(f)
        # rename_dict = mdata['colname_map']
        # df = df.rename(rename_dict, axis=1)

        # # get target cols defined by metadata file
        # target_cols = mdata['target_cols']

        #
        ### define feature cols:
        # assumes that all candidate features are the right-most columns in df
        # and the first feature column name is prefixed with `mordred_desc_`

        # find index of first feature col
        mordred_descs = [c for c in df.columns if 'mordred_desc' in c]
        first_feat_index = list(df.columns).index(mordred_descs[0])
        all_feats = list(df.columns[first_feat_index:df.shape[1]])

        # assign master_df pointer to dataset for easier copying in sensitivity runs
        master_df = df

        target_aliases = {
            'bp_c_coalesced': 'Boiling Point (deg C)',
            'fp_c_coalesced': 'Flash Point (deg C)',
            'mp_c_coalesced': 'Melting Point (deg C)',
            'cn_coalesced': 'Cetane Number',
            'dcn_coalesced': 'DCN',
            'ysi_coalesced': 'YSI',
        }
    # TODO: comment this start to the for-loop back in
    # for target in targets:
        for build_fn in best_build_funcs[target]:
            # TODO: remove temp for loop for sensitivity run over thresh vals
            # for cum_imp_thresh in [0.6, 0.65, 0.7, 0.75,  0.8, 0.85, 0.9, 0.95]:
            for cum_imp_thresh in [RF_IMP_FEAT_CUM_THRESH]:
                logger.info(f'Using cumulative imp. thresh: {cum_imp_thresh}')
                logger.info(f'Using architecture from build func: {build_fn}')

                target_alias = target_aliases[target]
                logger.info(f'Building model for target: {target}')
                df = master_df.copy(deep=True)

                # determine dimensionality reduction factor to use
                if dim_reduce_factor == -1:
                    dim_reduce_factor = prop_dim_reduce_factors[target]

                # determine outlier contamination factor to use
                if outlier_contam == -1:
                    outlier_contam = outlier_contam_factors[target]

                # drop rows with NA target values
                logger.info(f'Original shape of df: {df.shape}')
                df = df.dropna(subset=[target])
                logger.info(
                    f'Shape after dropping samples with NA target: {df.shape}'
                )

                # compute counts of unique values in feature cols
                feat_val_summaries = compute_column_summaries(df, all_feats)

                logger.info(f'Using dim_reduce_factor: {dim_reduce_factor}')
                logger.info(f'Using outlier_contam: {outlier_contam}')

                feats_to_keep = drop_trivial_feature_cols(
                    df, feat_val_summaries, dim_reduce_factor
                )

                # drop additional columns using RF variable importances
                if SELECT_FEATURES_BY_RF_IMPORTANCE:
                    feats_to_keep = drop_features_by_rf_feat_imp(
                        df, feats_to_keep, target=target,
                        cumulative_thresh=cum_imp_thresh,
                        n_estimators=RF_NTREE, n_jobs=N_JOBS
                    )

                feats_to_drop = [c for c in all_feats if c not in feats_to_keep]
                feat_cols = feats_to_keep

                # makelist of feature columns with non-stationary features
                n = len(feat_cols); m = len(all_feats)
                logger.info(f'Using {n}/{m} descriptors as features.')

                # drop feature columns flagged for removal
                df = df[[c for c in df.columns if c not in feats_to_drop]]
                logger.info(
                    f'Shape after feature removal: {df.shape}'
                )

                # drop samples with NA for any feature
                if not impute:
                    logger.info('No imputation being performed.')
                    logger.info('Dropping samples with any NA feature vals')
                    df = df.dropna(subset=feat_cols)
                    logger.info(
                        f'Samples with NA feature vals removed'
                        f'Shape of df: {df.shape}'
                    )
                elif impute == 'simple':
                    logger.info(
                        'Using simple mean imputation for missing feature vals'
                    )
                    imputer = SimpleImputer()
                    df[feat_cols] = imputer.fit_transform(df[feat_cols])


                iqr_outliers_removed = 0
                if IQR_OUTLIER_REMOVAL:
                    # identify and remove outliers based on target vals
                    logger.info(
                        f'Removing outliers based on target values using IQR method'
                    )
                    pre_shape = df.shape
                    iqr_fac = iqr_outlier_factors[target]
                    df = remove_outliers_by_iqr(df, target, iqr_fac)
                    logger.info(
                        f'Shape after removing outliers by IQR method: {df.shape}'
                    )
                    iqr_outliers_removed = pre_shape[0] - df.shape[0]

                iso_for_outliers_removed = 0
                if ISO_FOR_OUTLIER_REMOVAL:
                    # identify and remove outliers based on feature vals
                    logger.info(
                        f'Removing outliers based on feature values using Iso. Forest'
                    )
                    pre_shape = df.shape
                    df = remove_outliers_by_isolation_forest(
                        df, feat_cols, outlier_contam, target
                    )
                    logger.info(
                        f'Shape after removing outliers by Iso. Forest: {df.shape}'
                    )
                    iso_for_outliers_removed = pre_shape[0] - df.shape[0]

                # extract features and labels from modeling df
                features = df.loc[:, feat_cols]
                labels = df.loc[:, target]

                print()
                logger.info(f'(Target: {target})')
                logger.info(f'(Mean of target vals: {np.mean(labels)})')
                logger.info(f'(Std.Dev. of target vals: {np.std(labels)})')
                print()
                fig, ax = plt.subplots()
                sns.distplot(labels, ax=ax)
                ax.set_xlabel(target_alias)
                ax.set_ylabel('Frequency')

                text_str = (
                    f'Median = {np.median(labels):0.3f}'
                    f'\nMean = {np.mean(labels):0.3f}'
                    f'\nStd. Dev. = {np.std(labels):0.3f}'
                )

                # these are matplotlib.patch.Patch properties
                props = dict(boxstyle='round', facecolor='gray', alpha=0.5)
                ax.text(0.7, 0.95, text_str, transform=ax.transAxes,
                        fontsize=14,
                        verticalalignment='top', bbox=props)

                fig.savefig(f'../plots/distplot_{target}.png')

                # TODO: temp, remove
                # continue

                # simple train test split
                # X_train, X_test, y_train, y_test = train_test_split(features, labels,
                #                                                     test_size=test_size,
                #                                                     random_state=RANDOM_SEED
                #                                                     )

                n_features = features.shape[1]
                callbacks = define_callbacks()
                # init list of estimators to be used for building modeling pipeline
                estimators = [
                    ('scaler', StandardScaler()),
                    # ('feature_selection',
                    #  SelectFromModel(LinearSVR())
                    # ),
                    ('regressor', KerasRegressor(
                        build_fn=build_fn(n_features),
                        epochs=n_epochs,
                        batch_size=batch_size,
                        verbose=verbosity
                        )
                    )
                ]
                pipeline = Pipeline(estimators)

                # determine unique identifier for this net
                plid = str(datetime.now())[2:19]
                plid = plid.replace(':', '').replace(' ', '').replace('-', '')

                kfold_splitter = KFold(
                    n_splits=N_CV_FOLDS,
                    random_state=RANDOM_SEED,
                    shuffle=True
                )


                # note: running this plotting function calls the `fit` method on the
                # pipeline so it is computationally expensive
                plot_loss_history(pipeline, plid, features, labels, target_alias)

                # run cross validation to compute MSE values for each fold
                mse_vals = cross_val_score(
                    pipeline, features, labels,
                    cv=kfold_splitter,
                    scoring='neg_mean_squared_error'
                    # fit_params={'regressor__callbacks': callbacks}
                )
                # compute RMSE vals for folds
                rmse_vals = [np.sqrt(abs(x)) for x in mse_vals]

                # create barplot viz of fold CV scores
                fig, ax = plt.subplots()
                visualizer = KFoldCVScoresViz(
                    ax=ax,
                    cv=kfold_splitter,
                    cv_scores=rmse_vals,
                    score_metric='RMSE',
                    target=target_alias,
                    modtype='ANN',
                )
                visualizer.draw()
                visualizer.poof(outpath=f'../plots/kfold_cv_scores_plid_{plid}.png')

                # compute mean and standard deviation of fold RMSE values
                mean_rmse = np.mean(rmse_vals)
                std_dev_rmse = np.std(rmse_vals)

                kfold_metrics = {
                    'n_folds': N_CV_FOLDS,
                    'fold_mse_vals': list(mse_vals),
                    'fold_rmse_vals': list(rmse_vals),
                    'mean_rmse': mean_rmse,
                    'std_dev_rmse': std_dev_rmse,
                }
                logger.info('-' * 50)
                logger.info(f'Performance Metrics for: {target}')
                logger.info('-' * 50)
                logger.info(f'Mean RMSE: {mean_rmse}')
                logger.info(f'Std. Dev. RMSE: {std_dev_rmse}')
                logger.info(f'(Mean of target vals: {np.mean(labels)})')
                logger.info(f'(Std.Dev. of target vals: {np.std(labels)})')
                logger.info('-' * 50 + '\n')


                wts_fname = f'wts_{target}_eps_{n_epochs}_plid_{plid}.hdf5'
                wts_fpath = f'../neural_net_exports/{wts_fname}'

                if EXPORT_WEIGHTS:
                    res = pipeline.fit(features, labels, regressor__callbacks=callbacks)
                    res.named_steps['regressor'].model.save(wts_fpath)

                #
                # nn_model.load_weights(weights_fpath)
                # nn_model.compile(loss='mean_squared_error', optimizer='adam',
                #                  metrics=['mean_squared_error']
                # )
                #
                # # Make predictions on test data
                # preds = nn_model.predict(X_test)
                # preds = preds.flatten()
                # y_test = np.array(y_test).flatten()
                #
                # test_metrics = calc_perf_metrics(y_test, preds, omit=['r_squared'])
                ts = str(datetime.now())[:-10].replace(' ', '_')

                # generate model run results object
                mod_obj = {
                    'input_data_fname': DATA_FNAME,
                    'dim_reduce_factor': dim_reduce_factor,
                    'outlier_removal': {
                        'target': {
                            'iqr_method': {
                                'applied': str(rm_outliers_by_iqr),
                                'n_outliers_removed': iqr_outliers_removed
                            }
                        },
                        'features': {
                            'isolation_forest': {
                                'contamination': outlier_contam,
                                'n_estimators': ISO_FOR_NTREE,
                                'n_outliers_removed': iso_for_outliers_removed
                            }
                        }
                    },
                    'feature_selection': {
                        'rf_feat_importances': {
                            'cumulative_imp_thresh': RF_IMP_FEAT_CUM_THRESH,
                        }
                    },
                    'imputation': {
                        'approach': impute,
                    },
                    'plid': plid,
                    'weights_filename': wts_fname,
                    'build_fn': str(build_fn).split(' ')[1],
                    'eval_metrics': {
                            'kfold_metrics': kfold_metrics,
                    },
                    'validation_size': validation_size,
                    'n_epochs': n_epochs,
                    'timestamp': ts,
                    'output_units': output_units,
                    'target': target,
                    'n_samples': df.shape[0],
                    'n_features': len(feat_cols)
                }
                # print(mod_obj)

                mod_json_fp = f'../neural_net_exports/nn_mdata_plid_{plid}.json'
                with open(mod_json_fp, 'w') as f:
                    json.dump(mod_obj, f)


if __name__ == '__main__':
    main()
