""""
A note on dependencies:
tylerhuntington222's github fork of yellowbrick is required instead of
the main distribution from DistrictDataLabs.

To install with pip:
pip install git+https://github.com/tylerhuntington222/yellowbrick
"""
from yellowbrick.model_selection import CVScores
from sklearn.model_selection import KFold

import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np

class KFoldCVScoresViz(CVScores):

    def __init__(self, ax=None, cv_scores=None, score_metric='Score',
                 target='', modtype='Model',
                 **kwargs):
        super(KFoldCVScoresViz, self).__init__(ax, **kwargs)
        self.ax = ax
        self.cv_scores_ = cv_scores
        self.cv_scores_mean_ = np.mean(cv_scores)
        self.score_metric_ = score_metric
        self.target = target
        self.modtype = modtype

    def draw(self, **kwargs):
        """
        Creates the bar chart of the cross-validated scores generated from the
        fit method and places a dashed horizontal line that represents the
        average value of the scores.
        """

        width = kwargs.pop("width", 0.3)
        linewidth = kwargs.pop("linewidth", 1)

        xvals = np.arange(1, len(self.cv_scores_) + 1, 1)
        self.ax.bar(xvals, self.cv_scores_, width=width, color=self.color)
        hline_lab = (
            f'Mean {self.score_metric_} = {self.cv_scores_mean_:0.3f}'
            f'\n(Std. Dev. = {np.std(self.cv_scores_):0.3f})'
        )
        self.ax.axhline(
            self.cv_scores_mean_,
            color=self.color,
            label=hline_lab,
            linestyle="--",
            linewidth=linewidth,
        )

        return self.ax

    def finalize(self, **kwargs):
        """
        Add the title, legend, and other visual final touches to the plot.
        """

        # Set the title of the figure
        title = f'Cross Validation Scores for {self.target} {self.modtype}'
        self.set_title(title)

        # Add the legend
        loc = kwargs.pop("loc", "best")
        edgecolor = kwargs.pop("edgecolor", "k")
        self.ax.legend(frameon=True, loc=loc, edgecolor=edgecolor)

        # set spacing between the x ticks
        self.ax.xaxis.set_major_locator(ticker.MultipleLocator(1))

        # Set the axis labels
        self.ax.set_xlabel("Cross-Validation Fold")
        self.ax.set_ylabel(self.score_metric_)


def main():
    """
    Example usage
    """

    # populate dummy RMSE vals for five folds
    rmse_vals = [4.65, 7.89, 6.77, 5.29, 5.97]

    # init kfold splitter
    kfold_splitter = KFold(
        n_splits=5,
    )

    # create barplot viz of fold CV scores
    fig, ax = plt.subplots()
    visualizer = KFoldCVScoresViz(
        ax=ax,
        cv=kfold_splitter,
        cv_scores=rmse_vals,
        score_metric='RMSE',
        target='[Target]',
        modtype='[Model Type]',
    )
    visualizer.draw()
    visualizer.poof(outpath=f'kfold_cv_scores.png')

if __name__ == '__main__':
    main()