import json
import joblib
# load the pipeline metadata
pipeline_metadata_fp = '../tpot_search_results/tpot_pipeline_PLID_7117675_metadata.json'
with open(pipeline_metadata_fp, 'r') as f:
    pipeline_metadata = json.load(f)

serialized_pipeline_fp = pipeline_metadata['serialized_pipeline_fp']

exported_pipeline = joblib.load(serialized_pipeline_fp)
print(exported_pipeline.fitted_pipeline_)
