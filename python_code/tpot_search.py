import json
import argparse
import pandas as pd
import numpy as np
import pandas as pd
from ml_utils import calc_perf_metrics
from datetime import datetime
from tpot import TPOTRegressor
from sklearn.model_selection import train_test_split
from joblib import dump, load
from random import randint

global RANDOM_SEED
RANDOM_SEED = 222


def main():
    # configure command line args
    parser = argparse.ArgumentParser(
        description='Model search for an optimal TPOT pipeline.'
    )

    parser.add_argument('--max_search_minutes', '-m', default=10)

    parser.add_argument('--properties_data_fp', '-p',
        help='Path to .csv with properties data.',
        default='../data/cn_model_v2.0_full.csv'
    )

    parser.add_argument('--valid_size', '-v',
        help='Fraction of data to use as validation set (given as decimal)',
        default='0.2',
    )

    parser.add_argument('--test_size', '-ts',
        help='Fraction of data to use as test set (given as decimal)',
        default='0.1',
    )

    parser.add_argument('--target', '-tg',
        help="Variable to treat as target. Must be column in "\
             "properties data .csv",
        default='Target',
    )

    # parse command line args
    args = parser.parse_args()
    max_minutes = int(args.max_search_minutes)
    valid_size = float(args.valid_size)
    test_size = float(args.test_size)
    prop_data_fp = args.properties_data_fp
    target = args.target

    # load properties data into dataframe
    df = pd.read_csv(prop_data_fp, encoding="iso-8859-1")
    data_fname = prop_data_fp.split('/')[-1]
    
    # rename cols of superpro results df according to metadata JSON
    metadata_fp = f'{prop_data_fp[:-4]}_metadata.json'
    with open(metadata_fp, 'r') as f:
        metadata = json.load(f)
    rename_dict = metadata['colname_map']
    df = df.rename(rename_dict, axis=1)
    target_cols = metadata['target_cols']
    id_cols = metadata['id_cols']

    # define features and coerce to array
    feat_cols = [c for c in df.columns if c not in target_cols + id_cols]
    features = np.array(df.loc[:, feat_cols])

    # define labels and coerce to array
    labels = np.array(df.loc[:, f'{target}'])

    X_train, X_test, y_train, y_test = train_test_split(
        features, labels, test_size=test_size, random_state=RANDOM_SEED
    )

    # init TPOT regressor which will perform optimal model search
    tpot = TPOTRegressor(verbosity=2,
                         n_jobs=-1,
                         max_time_mins=max_minutes
    )

    # run TPOT search
    tpot.fit(X_train, y_train)

    # determine unique identifier for this pipeline
    plid = str(datetime.now())[2:19]
    plid = plid.replace(':', '').replace(' ', '').replace('-', '')

    # serialize and export optimal pipeline
    dir = '../serialized_tpot_pipelines/'
    serial_pipeline_fp = f'{dir}tpot_pipeline_{plid}.joblib'
    dump(tpot.fitted_pipeline_, serial_pipeline_fp)

    # create timestampt for this search result
    ts = str(datetime.now())[:-10]
    ts = ts.replace(' ', '_')

    # export pipeline metadata as JSON
    pipeline_metadata = {
        "plid": plid,
        "timestamp": ts,
        "max_searchtime_mins": max_minutes,
        "target": target,
        "serialized_pipeline_fp": serial_pipeline_fp,
        "properties_data_fp": prop_data_fp,
        "test_size": test_size,
        "random_seed": RANDOM_SEED
    }

    metadata_fp = f'../tpot_search_results/tpot_pipeline_{plid}_metadata.json'
    with open(metadata_fp, 'w') as f:
        json.dump(pipeline_metadata, f)

if __name__=='__main__':
    main()
