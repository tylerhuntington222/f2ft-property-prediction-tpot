import json
import joblib
import argparse
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_selection import SelectFwe, f_regression
from sklearn.svm import LinearSVR
from sklearn.preprocessing import (PolynomialFeatures, MinMaxScaler,
                                   FunctionTransformer
)
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.linear_model import LassoLarsCV, RidgeCV
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.kernel_approximation import Nystroem
from sklearn.model_selection import train_test_split
from sklearn.kernel_approximation import Nystroem
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import make_pipeline, make_union
from sklearn.svm import LinearSVR
from sklearn.metrics import mean_squared_error
from xgboost import XGBRegressor
from tpot.builtins import StackingEstimator
from ml_utils import *
from sklearn import linear_model
from copy import copy
sns.set()

# set randomization seed for reproducible results
global rand_seed
rand_seed = 222

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--fig_qual', '-fq',
        help="Quality of exported figures: int between 50-95.",
        default='95'
    )

    parser.add_argument('--fig_dpi', '-fd',
        help="DPI of exported figures.",
        default='300'
    )

    # parser.add_argument('--properties_data_fp', '-p',
    #     help='Path to .csv with properties data.',
    #     default='../data/cn_model_v2.0_full.csv'
    # )

    parser.add_argument('--pipeline_metadata_fp', '-p',
        help='Path to .json metadata file from a TPOT pipeline search',
        required=True
    )

    # parse command line args
    args = parser.parse_args()
    fig_qual = int(args.fig_qual)
    fig_dpi = int(args.fig_dpi)
    pipeline_metadata_fp = args.pipeline_metadata_fp

    # load the pipeline metadata
    print(pipeline_metadata_fp)
    with open(pipeline_metadata_fp, 'r') as f:
        pipeline_metadata = json.load(f)

    # extract pipeline properties
    plid = pipeline_metadata['plid']
    target = pipeline_metadata['target']
    serialized_pipeline_fp = pipeline_metadata['serialized_pipeline_fp']
    prop_data_fp = pipeline_metadata['properties_data_fp']
    test_size = pipeline_metadata['test_size']
    random_seed = pipeline_metadata['random_seed']

    # load properties data
    df = pd.read_csv(prop_data_fp, encoding="iso-8859-1")
    
    data_fname = prop_data_fp.split('/')[-1]
    # rename cols of superpro results df according to metadata JSON
    metadata_fp = f'{prop_data_fp[:-4]}_metadata.json'
    with open(metadata_fp, 'r') as f:
        metadata = json.load(f)
    rename_dict = metadata['colname_map']
    df = df.rename(rename_dict, axis=1)
    target_cols = metadata['target_cols']
    id_cols = metadata['id_cols']

    # define feature columns
    feat_cols = [c for c in df.columns if c not in target_cols + id_cols]
    features = np.array(df.loc[:, feat_cols])

    labels = np.array(df.loc[:, f'{target}'])

    X_train, X_test, y_train, y_test = train_test_split(
        features, labels, test_size=test_size, random_state=rand_seed
    )

    exported_pipeline = joblib.load(serialized_pipeline_fp)

    print('-'*60)
    print('Optimal TPOT Pipeline:')
    print(exported_pipeline)
    print('-'*60)

    # train model
    exported_pipeline.fit(X_train, y_train)

    # make predictions on test set
    preds = exported_pipeline.predict(X_test)

    # compute performance metrics on test set
    metrics = calc_perf_metrics(y_test, preds)

    metrics_str = (
        f"Test RMSE: {metrics['RMSE'].round(2)}\n"
        f"Test MAE: {metrics['MAE'].round(2)}\n"
        f"Test R^2: {metrics['r_squared'].round(2)}"
    )

    # create parity plot
    parity_plot(
        ax=None,
        labels=y_test,
        preds=preds,
        x_title = f'Experimental {target}',
        y_title = f'Predicted {target}',
        plot_title = f'Pipeline {plid} Parity Plot',
        tag=f'PLID_{plid}',
        textstr = metrics_str,
        export_fig=True
    )

    pipeline_metadata['model_eval'] = {'test_set_performance': metrics}
    with open(pipeline_metadata_fp, 'w') as f:
        json.dump(pipeline_metadata, f)



if __name__ == '__main__':
    main()
