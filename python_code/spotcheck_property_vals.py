"""
Script to check that coalesced values for each property are reasonably close
to original source values to detect possible data merging/coalescing errors
in database aggregation steps.
"""
import pandas as pd
import numpy as np

SD_MEAN_RATIO_THRESH = 0.1
NROWS = None

def main():
    # load data
    data_fname = 'fdf_9_5_19.csv'
    # read superpro results file and load into pd.dataframe
    data_fp = f'../data/{data_fname}'
    df = pd.read_csv(data_fp, nrows=NROWS)


    # drop feature cols
    df = df[[c for c in df.columns if 'mordred' not in c]]

    coal_prop_cols = [c for c in df.columns if 'coalesced' in c]

    # define targets and human-readable aliases
    target_aliases = {
        # 'bp_c_coalesced': 'Boiling Point (deg C)',
        'fp_c_coalesced': 'Flash Point (deg C)',
        # 'mp_c_coalesced': 'Melting Point (deg C)',
        # 'cn_coalesced': 'Cetane Number',
        # 'dcn_coalesced': 'DCN',
        # 'ysi_coalesced': 'YSI',
    }

    for target in target_aliases.keys():
        prop = target.replace('_coalesced', '')
        source_val_cols = [
            c for c in df.columns if prop in c
            and 'coalesced' not in c
            and 'diff' not in c
        ]
        df[f'{prop}_source_vals_mean'] = df[source_val_cols].mean(axis=1, skipna=True)
        df[f'{prop}_source_vals_sd'] = df[source_val_cols].std(axis=1, skipna=True)
        df[f'{prop}_sd_mean_ratio'] = \
            df[f'{prop}_source_vals_sd'] \
            / df[f'{prop}_source_vals_mean']

        source_val_cols.append(f'{prop}_source_vals_mean')
        source_val_cols.append(f'{prop}_source_vals_sd')
        source_val_cols.append(f'{prop}_sd_mean_ratio')
        sdf = df[df[f'{prop}_sd_mean_ratio'] > SD_MEAN_RATIO_THRESH]
        print(sdf[source_val_cols])
        print(source_val_cols)










if __name__ == '__main__':
    main()
