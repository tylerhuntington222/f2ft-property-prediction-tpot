from keras.datasets import mnist
import autokeras as ak
import sklearn.metrics
import absl.logging
import logging
import json
import argparse
import pandas as pd
import numpy as np
from ml_utils import calc_perf_metrics
from datetime import datetime
from collections import Counter
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.base import BaseEstimator
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.ensemble import IsolationForest
from sklearn.impute import SimpleImputer

from yellowbrick.model_selection import CVScores
from visualizers import KFoldCVScoresViz
import pandas as pd
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)

RANDOM_SEED = 42
N_CV_FOLDS = 10
ISO_FOR_NTREE = 100

# logging.basicConfig(level=logging.DEBUG)
# logger = logging.getLogger('dnn_builder')
# logger.setLevel(logging.DEBUG)
logging.root.removeHandler(absl.logging._absl_handler)
absl.logging._warn_preinit_stderr = False
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s',
                              datefmt='%m-%d %H:%M'
                              )
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

def find_feature_cols_to_drop(df, feat_col_summaries, dim_reduce_factor):
    """
    Returns a list of feature columns that should be dropped from df based
    on the dimensionality reduction factor passed by caller and
    other criteria contained within control logic of the funtion (i.e. features
    with a single unqiue value for all samples in df)

    Args:
        dim_reduce_factor (float):
            Threshold fraction of allowable NA values in a feature column
            for the feature to be considered (i.e. a value of 0.01 specifies
            thatfeatures for which 1% or more of values are NA should be
            omitted. Smaller values will therefore eliminate more features,
            thereby resulting in a greater number of samples with complete
            feature data, yielding a modeling dataset that is longer and
            narrower in shape. Larger values will retain more features at
            the expense of reducing the number of samples that have complete
            feature data, yielding a modeling dataset that is wider and
            shorter in shape.
    TODO: complete docstring
    """
    feat_cols_to_drop = []
    for colname, summary in feat_col_summaries.items():
        if summary['total_unique_vals'] == 1:
            feat_cols_to_drop.append(colname)

        if summary['fraction_nan_vals'] > dim_reduce_factor:
            feat_cols_to_drop.append(colname)

    # if np.isnan(df[f]).any():
    #     print(f'Dropping feature {f} with NA values')
    #     print(df[f].unique())
    #     feat_cols_to_drop.append(f)

    return feat_cols_to_drop


def compute_column_summaries(df, cols):
    """
    Converts all `cols` in df to numeric and returns dict of
    dicts (one for each feat_col) containing summary info such as the
    total count of nan values in the column and total number of unique
    values in the column.
    TODO: complete docstring
    """
    col_summaries_dict = {}

    for f in cols:
        df[f] = pd.to_numeric(df[f], errors='coerce')
        num_unique_vals = len(df[f].unique())
        total_nan_vals = np.isnan(df[f]).sum()
        col_vals_summary = {
            'total_nan_vals': total_nan_vals,
            'total_unique_vals': num_unique_vals,
            'fraction_nan_vals': total_nan_vals/df.shape[0]
        }
        col_summaries_dict[f] = col_vals_summary

    return col_summaries_dict


def remove_outliers_by_iqr(df, target):
    """
    Removes outliers using IQR method. Samples in df whose value in the `target`
    column are less than Q1 - (1.5 * IQR) or greater than Q3 + (1.5 * IQR)
    are removed from df.
    TODO: complete docstring
    """
    q1 = df[target].quantile(0.25)
    q3 = df[target].quantile(0.75)
    iqr = q3 - q1
    lo_bound = q1 - 1.5 * iqr
    hi_bound = q3 + 1.5 * iqr
    pre_shape = df.shape
    df = df[(df[target] > lo_bound) & (df[target] < hi_bound)]
    iqr_outliers_removed = pre_shape[0] - df.shape[0]
    logger.info(f'IQR Method removed {iqr_outliers_removed} outliers')
    return df

def remove_outliers_by_isolation_forest(df, feat_colnames, contamination):
    """
    Removes outliers using Isolation Forest method.
    TODO: complete docstring
    """
    iso_for = IsolationForest(n_estimators=ISO_FOR_NTREE,
                              contamination=contamination
                              )
    iso_for.fit(df[feat_colnames])
    ol_labs = iso_for.predict(df[feat_colnames])
    # remove samples considered outliers by isolation forest
    df = df[ol_labs == 1]
    counts = dict(Counter(ol_labs))
    logger.info(f'Isolation Forest with contamination factor of {contamination} '
                f'removed {counts[-1]} outliers'
                )
    return df

def plot_loss_history(model, plid, features, labels):
    history = model.fit(features, labels)
    history = history.named_steps['regressor'].model.history.history

    # summarize history for loss
    fig, ax = plt.subplots()
    ax.plot(history['loss'])
    ax.set_title('Model Loss History')
    ax.set_ylabel('Loss')
    ax.set_xlabel('Epoch')
    ax.legend(['Training'], loc='upper right')
    fig.savefig(f'../plots/loss_history_plid_{plid}.png')

def build_baseline_model(input_dim):
    def baseline_model():
        # build Neural Net
        nn_model = Sequential()

        # input layer
        nn_model.add(Dense(128, kernel_initializer='normal',
                           input_dim=input_dim,
                           activation='relu'
                           )
                     )

        # Hidden Layers
        nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))
        nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))
        nn_model.add(Dense(256, kernel_initializer='normal', activation='relu'))

        # Output layer
        nn_model.add(Dense(1, kernel_initializer='normal', activation='linear'))

        # Compile the network
        nn_model.compile(loss='mean_squared_error', optimizer='adam',
                         metrics=['mean_squared_error']
                         )
        return nn_model
    return baseline_model

def build_wider_model(input_dim, output_dim=1):
    def wider_model():

        # init neural net
        nn = Sequential()

        hidden_layer_neurons = int((2 * input_dim) / 3)

        # input layer
        nn.add(
            Dense(
                hidden_layer_neurons,
                kernel_initializer='normal',
                input_dim=input_dim,
                activation='relu'
            )
        )

        # hidden layers
        nn.add(
            Dense(
                hidden_layer_neurons,
                kernel_initializer='normal',
                activation='relu'
            )
        )

        # output layer
        nn.add(
            Dense(
                output_dim,
                kernel_initializer='normal',
                activation='linear'
            )
        )

        # compile the network
        nn.compile(
            loss='mean_squared_error',
            optimizer='adam',
            metrics=['mean_squared_error']
        )
        return nn
    return wider_model


def define_callbacks():
    callbacks = []
    # define a checkpoint callback
    checkpoint_fname = 'wts_mp_c_coalesced_ep_{epoch:03d}.hdf5'
    checkpoint_fpath = f'../neural_net_exports/{checkpoint_fname}'
    checkpoint = ModelCheckpoint(checkpoint_fpath, monitor='val_loss',
                                 save_best_only=True, mode='auto')
    callbacks.append(checkpoint)

    return callbacks

def main():

    # Prepare the data.
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train = x_train.reshape(x_train.shape + (1,))
    x_test = x_test.reshape(x_test.shape + (1,))

    # Search and train the classifier.
    clf = ak.ImageClassifier(verbose=True)
    clf.fit(x_train, y_train, time_limit=180)
    y = clf.predict(x_test, y_test)
    print(y)
    exit()



    # parse command line args
    parser = argparse.ArgumentParser(
        description='Script to build and evaluate a simple Deep Neural Net.'
    )

    parser.add_argument('--test_size', '-ts', action='store', default=0.2)
    parser.add_argument('--validation_size', '-v', action='store', default=0.2)
    parser.add_argument('--n_epochs', '-e', action='store', default=300)
    parser.add_argument('--batch_size', '-b', action='store', default=32)
    parser.add_argument('--target', '-tg',
                        help="Target variable to evaluate.",
                        default=''
                        )
    parser.add_argument('--input_units', '-u_in', action='store',
                        default='dol_per_lit')
    parser.add_argument('--output_units', '-u_out', action='store',
                        default='degrees C'
                        )
    parser.add_argument('--dim_reduce_factor', '-d', action='store',
                        default='-1'
                        )
    parser.add_argument('--outlier_contamination', '-oc', action='store',
                        default='0.0001'
                        )
    parser.add_argument('--build_fn_name', '-bfn', action='store',
                        default='build_wider_model'
                        )
    parser.add_argument('--verbosity', '-vb', action='store',
                        default='0'
                        )
    parser.add_argument('--rm_outliers_by_iqr', '-iqr', action='store',
                        default='1'
                        )
    parser.add_argument(
        '--impute', '-imp', action='store',
        help="Empty string (for no imputation) or one of: `simple`",
        default=''
    )

    prop_dim_reduce_factors = {
        'bp_c_coalesced': 0.0001,
        'fp_c_coalesced': 0.0001,
        'mp_c_coalesced': 0.0001,
        'cn_coalesced': 0.0001,
        'dcn_coalesced': 0.0001,
        'ysi_coalesced': 0.05,
    }

    outlier_contam_factors = {
        'bp_c_coalesced': 0.0001,
        'fp_c_coalesced': 0.0001,
        'mp_c_coalesced': 0.001,
        'cn_coalesced': 0.0001,
        'dcn_coalesced': 0.0001,
        'ysi_coalesced': 0.0001,
    }

    # TODO: perform lookups in this dict when building NN for each target
    impute_approaches = {
        'bp_c_coalesced': '',
        'fp_c_coalesced': '',
        'mp_c_coalesced': '',
        'cn_coalesced': 'simple',
        'dcn_coalesced': '',
        'ysi_coalesced': '',
    }

    args = parser.parse_args()
    n_epochs = int(args.n_epochs)
    logger.info(f'DNN training will last {n_epochs} epochs')
    batch_size = int(args.batch_size)
    logger.info(f'Batch size: {batch_size}')
    # default to list of all properties as targets if none specified as CL arg
    if not args.target:
        targets = [
            'ysi_coalesced',
            'dcn_coalesced',
            'mp_c_coalesced',
            'bp_c_coalesced',
            'cn_coalesced',
            'fp_c_coalesced',
        ]
    # put user-specified target into list to match default data structure
    else:
        targets = [args.target]

    # TODO: make this a dict lookup based on the target
    output_units = args.output_units
    test_size = float(args.test_size)
    validation_size = float(args.validation_size)
    outlier_contam = float(args.outlier_contamination)
    impute = args.impute
    verbosity = int(args.verbosity)
    rm_outliers_by_iqr = bool(int(args.rm_outliers_by_iqr))

    dim_reduce_factor = float(args.dim_reduce_factor)

    # parse name of build function to determine model architecture
    build_fn_name = args.build_fn_name

    # define build function mappings for available architectures
    build_funcs = {
        'build_baseline_model': build_baseline_model,
        'build_wider_model': build_wider_model
    }

    # lookup user specified build function and fail if doesnt exits
    try:
        build_fn = build_funcs[build_fn_name]
    except KeyError:
        e = f"""
            `{build_fn_name}` is not an available build function!
            Must be one of: 
            {[k for k in build_funcs.keys()]}
            """
        raise Exception(e)



    # load data
    data_fname = '../data/fdf_8_26_19.csv'
    # read superpro results file and load into pd.dataframe
    data_fp = f'../data/{data_fname}'
    df = pd.read_csv(data_fp)
    # rename cols of superpro results df according to metadata JSON
    fp = f'{data_fp[:-4]}_metadata.json'
    with open(fp, 'r') as f:
        mdata = json.load(f)
    rename_dict = mdata['colname_map']
    df = df.rename(rename_dict, axis=1)

    # # define target cols
    # target_cols = mdata['target_cols']

    # define feature cols
    feat_cols = [c for c in df.columns if 'mordred_desc' in c]


    # assign master_df pointer to dataset for easier copying in sensitivity runs
    master_df = df
    # TODO: remove this temp for-loop
    # for dim_reduce_factor in list(np.linspace(0.38, 0.65, 20)):
    # for outlier_contam in list(np.linspace(0.00001, 0.001, 20)):
    # for outlier_contam in [outlier_contam]:
    target_aliases = {
        'bp_c_coalesced': 'Boiling Point (deg C)',
        'fp_c_coalesced': 'Flash Point (deg C)',
        'mp_c_coalesced': 'Melting Point (deg C)',
        'cn_coalesced': 'Cetane Number',
        'dcn_coalesced': 'DCN',
        'ysi_coalesced': 'YSI',
    }
    for target in targets:

        target_alias = target_aliases[target]
        logger.info(f'Building model for target: {target}')
        df = master_df.copy(deep=True)

        # determine dimensionality reduction factor to use
        if dim_reduce_factor == -1:
            dim_reduce_factor = prop_dim_reduce_factors[target]

        # determine outlier contamination factor to use
        if outlier_contam == -1:
            outlier_contam = outlier_contam_factors[target]

        # drop rows with NA target values
        logger.info(f'Original shape of df: {df.shape}')
        df = df.dropna(subset=[target])
        logger.info(
            f'Shape after dropping samples with NA target val: {df.shape}'
        )

        # compute counts of unique values in feature cols
        feat_val_summaries = compute_column_summaries(df, feat_cols)

        logger.info(f'Training with dim_reduce_factor: {dim_reduce_factor}')
        logger.info(f'Training with outlier_contam: {outlier_contam}')

        feat_cols_to_drop = find_feature_cols_to_drop(
            df, feat_val_summaries, dim_reduce_factor
        )

        # re-construct list of feature columns with non-stationary features
        n_desc = len(feat_cols)
        feat_cols = [f for f in feat_cols if f not in feat_cols_to_drop]
        logger.info(f'Using {len(feat_cols)}/{n_desc} descriptors as features.')

        # drop feature columns flagged for removal
        df = df[[c for c in df.columns if c not in feat_cols_to_drop]]
        logger.info(
            f'Shape after feature removal: {df.shape}'
        )

        # drop samples with NA for any feature
        if not impute:
            logger.info('No imputation being performed.')
            logger.info('Dropping samples with any NA feature vals')
            df = df.dropna(subset=feat_cols)
            logger.info(
                f'Shape after removing samples with NA feature vals: {df.shape}'
            )
        elif impute == 'simple':
            logger.info('Using simple mean imputation for missing feature vals')
            imputer = SimpleImputer()
            df[feat_cols] = imputer.fit_transform(df[feat_cols])


        # identify and remove outliers based on target vals
        logger.info(
            f'Removing outliers based on target values using 1.5*IQR method'
        )
        pre_shape = df.shape
        df = remove_outliers_by_iqr(df, target)
        logger.info(
            f'Shape after removing outliers by 1.5*IQR method: {df.shape}'
        )
        iqr_outliers_removed = pre_shape[0] - df.shape[0]

        # identify and remove outliers based on feature vals
        logger.info(
            f'Removing outliers based on feature values using Iso. Forest'
        )
        pre_shape = df.shape
        df = remove_outliers_by_isolation_forest(df, feat_cols, outlier_contam)
        logger.info(
            f'Shape after removing outliers by Iso. Forest: {df.shape}'
        )
        iso_for_outliers_removed = pre_shape[0] - df.shape[0]

        # extract features and labels from modeling df
        features = df.loc[:, feat_cols]
        labels = df.loc[:, target]

        # Build model and train.
        # Prepare the data.
        # x_train = x_train.reshape(x_train.shape + (1,))
        # x_test = x_test.reshape(x_test.shape + (1,))

        # simple train test split
        X_train, X_test, y_train, y_test = train_test_split(
            features, labels,
            test_size=test_size,
            random_state=RANDOM_SEED
        )
        X_train = np.array(X_train).reshape(X_train.shape + (1,))
        X_test = np.array(X_test).reshape(X_test.shape + (1,))

        # Search and train the classifier.
        automod = ak.ImageRegressor()
        # automod = ak.ImageRegressor()
        automod.fit(
            X_train,
            y_train,
            # validation_split=0.2,
            # verbosity=2,
            # n_epochs=n_epochs,
            # batch_size=batch_size
        )
        y_test_preds = automod.predict(X_test, y_test)
        mse = sklearn.metrics.mean_squared_error(y_test, y_test_preds)
        rmse = np.sqrt(mse)
        print(f'RMSE: {rmse}')
        continue


        # init list of estimators to be used for building modeling pipeline
        estimators = []
        estimators.append(('scaler', StandardScaler()))
        n_features = features.shape[1]
        callbacks = define_callbacks()
        keras_regressor = KerasRegressor(build_fn=build_fn(n_features),
                                         epochs=n_epochs,
                                         batch_size=batch_size,
                                         verbose=verbosity,
                                         )
        estimators.append(('regressor', keras_regressor))
        pipeline = Pipeline(estimators)

        # determine unique identifier for this net
        plid = str(datetime.now())[2:19]
        plid = plid.replace(':', '').replace(' ', '').replace('-', '')

        kfold_splitter = KFold(n_splits=N_CV_FOLDS,
                               random_state=RANDOM_SEED
                               )

        # note: running this plotting function calls the `fit` method on the
        # pipeline so it is computationally expensive
        plot_loss_history(pipeline, plid, features, labels)

        # run cross validation to compute MSE values for each fold
        mse_vals = cross_val_score(
            pipeline, features, labels,
            cv=kfold_splitter,
            # fit_params={'regressor__callbacks': callbacks}
        )
        # compute RMSE vals for folds
        rmse_vals = [np.sqrt(abs(x)) for x in mse_vals]

        # create barplot viz of fold CV scores
        fig, ax = plt.subplots()
        visualizer = KFoldCVScoresViz(
            ax=ax,
            cv=kfold_splitter,
            cv_scores=rmse_vals,
            score_metric='RMSE',
            target=target_alias,
            modtype='ANN',
        )
        visualizer.draw()
        visualizer.poof(outpath=f'../plots/kfold_cv_scores_plid_{plid}.png')

        # compute mean and standard deviation of fold RMSE values
        mean_rmse = np.mean(rmse_vals)
        std_dev_rmse = np.std(rmse_vals)

        kfold_metrics = {
            'n_folds': N_CV_FOLDS,
            'fold_mse_vals': list(mse_vals),
            'fold_rmse_vals': list(rmse_vals),
            'mean_rmse': mean_rmse,
            'std_dev_rmse': std_dev_rmse,
        }
        logger.info('-' * 50)
        logger.info(f'Performance Metrics for: {target}')
        logger.info('-' * 50)
        logger.info(f'Mean RMSE: {mean_rmse}')
        logger.info(f'Std. Dev. RMSE: {std_dev_rmse}')
        logger.info(f'(Mean of target vals: {np.mean(labels)})')
        logger.info(f'(Std.Dev. of target vals: {np.std(labels)})')
        logger.info('-' * 50 + '\n')


        wts_fname = f'wts_{target}_eps_{n_epochs}_plid_{plid}.hdf5'
        wts_fpath = f'../neural_net_exports/{wts_fname}'

        # TODO: refeactor this into global or CL arg
        export_wts = True
        if export_wts:
            res = pipeline.fit(features, labels, regressor__callbacks=callbacks)
            res.named_steps['regressor'].model.save(wts_fpath)

        #
        # nn_model.load_weights(weights_fpath)
        # nn_model.compile(loss='mean_squared_error', optimizer='adam',
        #                  metrics=['mean_squared_error']
        # )
        #
        # # Make predictions on test data
        # preds = nn_model.predict(X_test)
        # preds = preds.flatten()
        # y_test = np.array(y_test).flatten()
        #
        # test_metrics = calc_perf_metrics(y_test, preds, omit=['r_squared'])
        ts = str(datetime.now())[:-10].replace(' ', '_')

        # generate model run results object
        mod_obj = {
            'dim_reduce_factor': dim_reduce_factor,
            'outlier_removal': {
                'target': {
                    'iqr_method': {
                        'applied': str(rm_outliers_by_iqr),
                        'n_outliers_removed': iqr_outliers_removed
                    }
                },
                'features': {
                    'isolation_forest': {
                        'contamination': outlier_contam,
                        'n_estimators': ISO_FOR_NTREE,
                        'n_outliers_removed': iso_for_outliers_removed
                    }
                }
            },
            'imputation': {
                'approach': impute,
            },
            'plid': plid,
            'weights_filename': wts_fname,
            'build_fn': str(build_fn).split(' ')[1],
            'eval_metrics': {
                'kfold_metrics': kfold_metrics,
            },
            'validation_size': validation_size,
            'n_epochs': n_epochs,
            'timestamp': ts,
            'output_units': output_units,
            'target': target,
            'n_samples': df.shape[0],
            'n_features': len(feat_cols)
        }
        # print(mod_obj)

        mod_json_fp = f'../neural_net_exports/nn_mdata_plid_{plid}.json'
        with open(mod_json_fp, 'w') as f:
            json.dump(mod_obj, f)


if __name__ == '__main__':
    main()
