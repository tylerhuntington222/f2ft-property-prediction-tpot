## TPOT Toolkit for Fuel Property Prediction

This repository contains a set of tools for leveraging
the [Tree-Based Pipeline Optimization Tool]("https://epistasislab.github.io/tpot/")
(TPOT), for predicting properties of fuel molecules.
TPOT is an Automated ML framework that provides an interface for
searching and evaluating a broad
range of ML algorithms to ultimately identify a model
architecture and set of hyperparameters that achieves the
lowest internal
[cross-validation]("https://en.wikipedia.org/wiki/Cross-validation_(statistics)")
prediction error.
Oftentimes, multiple ML algorithms are combined using a technique
called [ensembling]("https://en.wikipedia.org/wiki/Ensemble_learning")
to obtain the best prediction accuracy.

A machine learning workflow based upon TPOT can be broken down into
two key stages: (1) searching for an optimal pipeline
for a given a dataset of features and labels, and (2)
evaluating the test performance of the optimal
pipeline using a train/test
split of the original data.

This toolkit provides scripts that can be run from the command line to automate
these stages.

### Requirements
An installation of Python 3.6 or later is required to run all code in this
repository. Additionally, the packages (with associated version numbers) listed
in `requirements.txt` must be installed. The easiest way to get up and
running is to create a Python 3.6 virtual environment and install the
dependencies in batch using the following series of commands from within
this repository's root directory:

(The first command is only necessary if you do not have `virtualenv` installed)

```
$ pip install virtualenv
```

```
$ virtualenv --python=python3.6 py36env
```

```
$ source py36env/bin/activate
```

```
$ pip install -r requirements.txt
```

Assuming all these commands run smoothly, you should be set up to run
all the scripts as outlined below. When you are finished working inside this
project repository, you can deactivate the virtual environment
you just created (`py36env`) by running the command:

```
$ deactivate
```


### Step 1: Finding an Optimal Pipeline

The script `tpot_search.py` performs the first step: finding an optimal
pipeline. The following command line arguments may be provided:

####  Command Line Arguments
|Flag |Default                |Description
|-----|-----------------------|----------------------------------------
|`-h` |                       |Show this help message and exit
|`-m` |`5`                      |Maximum allowable search time (minutes) to let TPOT run.
|`-p` |`../data/cn_model_v2.0_full.csv`| Filepath to CSV file with properties data.
|`-v` |`0.2`| Fraction of data to use for validation during model training.
|`-ts` |`0.1`| Fraction of data to set aside for test set to evaluate model performance after pipeline optimization.
|`-tg` |`Target`| Target prediction variable (must match a column name in properties data CSV).

#### Example Usage
```
$ python tpot_search.py -m 60 -p ../data/fuel_props.csv -v 0.25 -tg cetane_num
```

Note that default values are provided for all command line arguments (see
table above), so running
```
$ python tpot_search.py
```
will work using those defaults.

In addition to the CSV file that is specified by the `-p` flag, an
accompanying JSON metadata file is also required for `tpot_search.py` to run.
This file should be located in the same directory as the CSV file and
conform to the following naming convention:
`<csv-file-name>_meatadata.json` where `<csv-filename>` is the name of
the properties data CSV without the `.csv` extension. The metadata JSON is used
when loading the properties dataset and contains the following sections:

`colname_map`: Optional mappings of native column names in input CSV to names
that should be used for those columns in all downstream data processing steps.

`target_cols`: Required list of column name(s) in the input CSV (or their mapped
values in the `colname_map` section if provided) denoting variables that
are prediction targets (i.e. labels, not features in the dataset).

`id_cols`: Optional list of identifier column names in the input CSV whose
values should be regarded as neither features or labels in model training.

An example of such a metadata file (`../data/cn_model_v2.0_full_metadata.json`)
is provided below for reference:
```JSON
{
  "colname_map": {
      "DATAID": "DATA_ID"
  },
  "target_cols": [
    "Target"
  ],
  "id_cols": [
    "DATA_ID",
    "ASSIGNMENT",
    "Compound Name",
    "SMILES",
    "Formula",
    "Compound Group",
    "CG2",
    "Source",
    "Method"
  ]
}
```

Running `tpot_search.py` as shown above will initiate the TPOT optimization
process which will run for as long as specified by the `-m` argument. Once
the maximum allowable minutes have elapsed, the optimal pipeline (i.e. that
which has been found to yield the lowest internal cross-validation score) will
be serialized (using [`joblib`](https://joblib.readthedocs.io/en/latest/auto_examples/serialization_and_wrappers.html))
and saved to the directory `../serialized_tpot_pipelines`.
Additionally, a JSON metadata file describing the search parameters used
and providing references to the input data file and serialized pipeline will
be saved to the `../tpot_search_results/` directory. The naming conventions
used for the serialized pipeline object and the metadata files are as follows:

**Serialized Pipelines:**

`tpot_pipeline_PLID_<unique-plid>.joblib`

**Pipeline Metadata:**

`tpot_pipeline_PLID_<unique-plid>_metadata.json`


where `<unique-plid>` is replaced with a twelve digit code that uniquely
identifies the given pipeline. The contents of an example JSON metadata file
are provided below:

```JSON
{
  "plid": "PLID_190527143322",
  "timestamp": "2019-04-25_12:01",
  "max_searchtime_mins": 200,
  "properties_data_fp": "../data/cn_model_v2.0_full.csv",
  "target": "Target",
  "serialized_pipeline_fp": "../serialized_tpot_pipelinestpot_pipeline_PLID_190527143322.joblib",
  "test_size": 0.1,
  "random_seed": 222,
}
```


### Step 2: Evaluating Pipeline Performance

The script `evaluate_pipeline.py` performs the second stage of the TPOT
machine learning workflow: evaluating the prediction accuracy of a
pipeline on unseen data. This script also generates a parity plot which
provides a useful visualization of the model's performance, along with
computing standard error metrics including MAE, MSE, and RMSE.  

####  Command Line Arguments
|Flag |Default                |Description
|-----|---------|----------------------------------------
|`-h` |         |Show this help message and exit
|`-p` |         |Filepath to pipeline metadata file describing the pipline to be evaluated.|
|`-fq` |`95`   |Quality of exported figures. Integer between 50 - 95.
|`-fd` |`300`    |DPI of exported figures. Integer between 72 - 1200.

#### Example Usage
```
$ python evaluate_pipeline.py -p ../exported_tpot_pipelines/my_pipeline.json -fq 80 -fd 200
```

Note that default values are provided for all command line arguments **except**
for `-p` (the pipeline metadata file). A valid filepath must be provided
in order for the script to run. A minimal working example is provided below:

```
$ python tpot_search.py -p ../exported_tpot_pipelines/my_pipeline.json
```

Running `evaluate_pipeline.py` will evaluate the prediction accuracy of
the pipeline on the holdout test data and produce the following
outputs/side-effects:

1) Summary of performance metrics in the terminal.

2) Summary of performance metrics will be nested within the metadata file
of the evaluated pipeline. An example of such an updated JSON metadata file
is provided below:

```JSON
{
  "plid": "PLID_190527143322",
  "timestamp": "2019-04-25_12:01",
  "max_searchtime_mins": 200,
  "properties_data_fp": "../data/cn_model_v2.0_full.csv",
  "target": "Target",
  "serialized_pipeline_fp": "../serialized_tpot_pipelinestpot_pipeline_PLID_190527143322.joblib",
  "test_size": 0.1,
  "random_seed": 222,
  "model_eval": {
    "test_set_performance": {
      "MSE": 105.59937573277105,
      "RMSE": 10.27615568842605,
      "MAE": 8.037970068027212,
      "accuracy": 0.7892911922,
      "r_squared": 0.8853697069911993,
    }
  }
}
```

3) Parity plot showing prediction accuracy of the model on the test set will
be exported to the `../plots/` directory with the following filename:
`parity_plot-PLID_<unique-plid>.png`.
