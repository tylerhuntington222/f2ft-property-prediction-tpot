import json
import os
import pprint as pp

pp = pp.PrettyPrinter(indent=4)

TARGET = 'fp_c_coalesced'
VERBOSE = False

def main():
    print('START\n')

    files = [f for f in os.listdir() if f.endswith('.json')]
    for fname in files:
        try:
            with open(fname, 'r') as f:
                data = json.load(f)

            if data['target'] == TARGET and data['n_epochs'] == 360 and 'plid' in data:
            #if data['target'] == TARGET and 'plid' in data:
                print(f'Loading JSON file: {fname}')
                pp.pprint(data['plid'])
                pp.pprint(data['build_fn'])
                pp.pprint(data['timestamp'])
                pp.pprint(data['eval_metrics']['kfold_metrics']['mean_rmse'])
                print()
        except json.JSONDecodeError:
            if VERBOSE:
                print(f'Error: unable to open {fname}! Invalid JSON')
    print('FINSIH')




if __name__ == '__main__':
    main()
