# Deep Learning Models for Property Prediction

## Overview

Several neural net architectures have been developed for predicting fuel 
properties from compound descriptors.

A MongoDB instance has been created to store metadata, network weights and
evaluation metrics of trained models to facilitate the search for and
reproducibility of optimal modeling configurations.

For each neural net architecture, separate data preprocessing and model training 
pipelines have been been performed to predict each of the following properties:

`cn`: Cetane Number

`dcn`: Derived Cetane Number

`mp_c`: Melting Point in degrees Celsius

`bp_c`: Boiling Point in degrees Celsius

`fp_c`: Flash Point in degrees Celsius

`ysi`: Yield Sooting Index

## Data Preprocessing

##### Outlier Removal
In each pipeline, two methods are used to remove outliers from the 
working dataset used for model training and validation. 

First, we use the 1.5\*IQR method to remove samples that are considered outliers
on the basis of target (property) values that are significantly distant from the
mid-region of their distribution. 
That is, samples with a target value that is either 
less than Quartile1 - (1.5\*IQR) or greater than Quartile3 + (1.5*IQR) are
dropped (where IQR = Quartile3 - Quartile1).

Secondly, we use Isolation Forests to remove outliers on the basis of feature
combinations that stray from the central regions of clustering patterns in 
the feature space. The Isolation Forest algorithm takes a `contamination` 
hyperparameter which the proportion of outliers in the data set and is used when 
in the fitting routine to define the threshold on the decision function for 
distinguishing outliers from inliers. More information on Isolation Forests can
be found here: 
https://scikit-learn.org/stable/modules/outlier_detection.html#isolation-forest.

##### Feature Selection
Descriptors for which there was no variation in values accross samples were
automatically removed.

A dimensionality reduction factor is applied to remove additional descriptors
with a proportion of null values above a certain threshold.  
In general, smaller dimensionality reduction factors 
eliminate more features,
thereby resulting in a greater number of samples with complete
feature data and a trainable dataset that is longer and
narrower in shape.  Larger factors, on the other hand, 
will retain more features at
the expense of reducing the number of samples with complete
feature data, resulting in a trainable dataset that is wider and
shorter in shape. For our purposes, it appears that retaining more samples
at the expense of losing features allows for training the most performant 
models.
Specifically, we have tuned this factor to 
tuned to values between 0.0001 - 0.05 (depending on target) for
our pipelines.

##### Feature Scaling
After outlier removal and prior to model training, all features are centered on
their means and scaled to unit variance using scikit-learn's `StandardScaler`. 
Centering and scaling is performed separately on each feature by computing the 
relevant statistics on samples in the training set. 
In the case of cross-fold validation (which we use for model evaluation) the 
training portion of each fold undergoes scaling independently prior to model
fitting, prediction and scoring. 

##### Model Evaluation
We use k-fold cross-validation (CV) for evaluating model performance because of the
observation that performance varies substantially depending on the nature of 
how training and test data are split. We compute Root Mean Squared Error (RMSE) 
on each validation fold during CV use the average and standard deviation of 
these values as the primary metric of model performance.

## Results

Preliminary results for a baseline ANN architecture with one hidden layer 
trained for predicting each key property of interest are shown below.

#### Yield Sooting Index (YSI)
![](../plots/distplot_ysi_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190905231052.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190905231052.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.


#### Direct Cetane Number (DCN)
![](../plots/distplot_dcn_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190905232956.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190905232956.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.

#### Cetane Number (CN)
![](../plots/distplot_cn_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190906003307.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190906003307.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.


#### Melting Point
![](../plots/distplot_mp_c_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190910140834.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190910140834.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.



#### Boiling Point
![](../plots/distplot_bp_c_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190906012615.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190906012615.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.


#### Flash Point
![](../plots/distplot_fp_c_coalesced.png)
Figure 1: Distribution of compound property values after removal of outliers by
1.5\*IQR and Isolation Forest methods.

![](../plots/loss_history_plid_190906030853.png)
Figure 2: Loss history curve over number of training epochs.

![](../plots/kfold_cv_scores_plid_190906030853.png)
Figure 3: Root Mean Squared Error (RMSE) values
computed on holdout sets of 5-fold cross 
validation.

### Summary

| Property        | Target Dist. SD   | 5-Fold Mean RMSE | Error Ratio\*|
| ------------- | ------------- | ----- | --- |
| Yield Sooting Index | 112.3 | 79.9 | 0.711 |
| Derived Cetane Number     | 28.19 |   16.6 | 0.589 |
| Cetane Number | 26.53      |    12.7 | 0.479|
| Melting Point | 89.32 | 52.4 | 0.589 | 
| Boiling Point | 11.56     |    5.68 | 0.491 |
| Flash Point | 61.73   |    44.5 | 0.721 |


\*Error Ratio = 5-Fold Mean RMSE / Target Dist. SD 


    
 
    
 
### Updates based on 09_19_19 version of DB

Boiling point prediction accuracy is much worse on
this version of the database (RMSE ~45 deg C) compared to the previous
version (RMSE ~7 deg C).
This appears to be mainly due a substantial reduction in sample size 
(i.e. compounds 
with a boiling point value in the dataset). Whereas the previous version of 
the DB had ~16000 samples with BP values, the 09_19_19 version has only
~3900.





